{
  description = "Nofreedisk NixOS cluster";

  nixConfig = {
    bash-prompt-suffix = "\\[\\033[01;33m\\](nofreedisk)\\[\\033[00m\\] ";
    extra-experimental-features = "nix-command flakes";
    allowUnfree = true;
    sandbox = "relaxed";
#    bash-prompt-prefix = "❄ ";
  };

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixpkgs-docker.url = "github:NixOS/nixpkgs/81ed90058a851eb73be835c770e062c6938c8a9e";
    nixos-images = {
      url = "github:nix-community/nixos-images"; # Has cache.
      inputs.nixos-2311.follows = "nixpkgs";
      inputs.nixos-unstable.follows = "nixpkgs";
    };
#    srvos.url = "github:numtide/srvos";
    nixos-anywhere = {
      url = "github:numtide/nixos-anywhere"; # Has no cache.
      inputs.disko.follows = "disko";
      inputs.nixos-stable.follows = "nixpkgs";
      inputs.nixos-images.follows = "nixos-images";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    disko = {
      url = "github:nix-community/disko"; # Has no cache.
      inputs.nixpkgs.follows = "nixpkgs";
    };
    deploy-rs = {
      url = "github:serokell/deploy-rs"; # Has no cache.
      inputs.nixpkgs.follows = "nixpkgs";
    };
    sops-nix = {
      # https://github.com/Mic92/sops-nix/pull/338
#      url = "github:Mic92/sops-nix"; # Has cache.
      url = "github:thomaslepoix/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nixpkgs-stable.follows = "nixpkgs";
    };
  };

  outputs = { self
  , nixpkgs
  , nixpkgs-unstable
  , nixpkgs-docker
  , disko
  , deploy-rs
  , nixos-anywhere
  , sops-nix
  , ...
  }@attrs:
  let
    pkgs = (import nixpkgs { inherit system; config.allowlistedLicenses = [ lib.licenses.bsl11 ]; }).extend self.overlays.pkgs;
    pkgs-unstable = import nixpkgs-unstable { inherit system; config.allowlistedLicenses = [ lib.licenses.bsl11 ]; };
    pkgs-docker = nixpkgs-docker.legacyPackages.${system};
    lib = nixpkgs.lib.extend self.overlays.lib;
    system = "x86_64-linux";
    state-version = "23.11";

    sops-secrets = lib.decrypt [
      ./secrets/privates.json
     ];

    privates = builtins.fromJSON (builtins.readFile sops-secrets."privates.json");

    inherit (privates)
      authorized-keys
      dns-email
      ip-dev-node1
      ip-dev-node2
      ip-prod-node1 # Dauliac's server
      ip-prod-node2; # thomaslepoix' server

    terraform = pkgs-unstable.terraform_1.withPlugins (p: [
      p.consul
      p.nomad
      p.sops
      p.vault
    ]);

    deploy = deploy-rs.packages.${system}.deploy-rs;
    nixos-anywhere = attrs.nixos-anywhere.packages.${system}.nixos-anywhere;
    nixos-images = attrs.nixos-images.packages.${system}.kexec-installer-nixos-2311;

  in {
    overlays = {
      lib = final: prev: self.lib;
      pkgs = final: prev: {
        nomad = pkgs-unstable.nomad_1_6;
        consul = pkgs.callPackage ./pkgs/consul { inherit (pkgs-unstable) consul; };
        # https://discourse.nixos.org/t/docker-run-and-docker-exec-error/30848
        docker = pkgs-docker.docker;
        colored-webpage = pkgs.callPackage ./pkgs/colored-webpage {};
      };
    };

    lib = {
      flake-root = import ./lib/flake-root.nix { inherit lib pkgs; };
      tmpdir = import ./lib/tmpdir.nix { inherit lib; };
      sops-age-key-file = import ./lib/sops-age-key-file.nix { inherit lib pkgs; };
      decrypt = import ./lib/decrypt.nix { inherit self lib pkgs; };

      make-task = import ./lib/tasks/make-task.nix { inherit pkgs; };

      create-vanilla-secrets = import ./lib/tasks/create-vanilla-secrets.nix { inherit lib pkgs; };
      update-secrets = import ./lib/tasks/update-secrets.nix { inherit lib pkgs; };
      rotate-age-admin-key = import ./lib/tasks/rotate-age-admin-key.nix { inherit lib pkgs; };

      bootstrap-dev = import ./lib/tasks/bootstrap-dev.nix { inherit lib pkgs nixos-images nixos-anywhere; };
      bootstrap-prod = import ./lib/tasks/bootstrap-prod.nix { inherit lib nixos-anywhere; };
      deploy = import ./lib/tasks/deploy.nix { inherit lib pkgs deploy; };
      ssh = import ./lib/tasks/ssh.nix { inherit lib; };
      sendkey-sops-dev = import ./lib/tasks/sendkey-sops-dev.nix { inherit lib pkgs; };
      keygen-sops-dev = import ./lib/tasks/keygen-sops-dev.nix { inherit lib pkgs; };
      keygen-sops-prod = import ./lib/tasks/keygen-sops-prod.nix { inherit lib pkgs; };
      rotate-consul-gossip-key = import ./lib/tasks/rotate-consul-gossip-key.nix { inherit lib pkgs; };
      rotate-consul-acl-initial-management = import ./lib/tasks/rotate-consul-acl-initial-management.nix { inherit lib pkgs; };
      rotate-consul-tls-ca = import ./lib/tasks/rotate-consul-tls-ca.nix { inherit lib pkgs; };
      rotate-consul-tls-certs = import ./lib/tasks/rotate-consul-tls-certs.nix { inherit lib pkgs; };

      terraform = import ./lib/tasks/terraform.nix { inherit lib pkgs terraform; };
      vagrant = import ./lib/tasks/vagrant.nix { inherit lib pkgs; };
      media = import ./lib/tasks/media.nix { inherit lib pkgs; };
    };

    nixosModules = {
      load-private-key = ./modules/load-private-key.nix;
      sops-example = ./modules/sops-example.nix;
      consul-rotate-gossip-encryption-key = ./modules/consul-rotate-gossip-encryption-key.nix;
      consul-reset-acl = ./modules/consul-reset-acl.nix;
      consul-connect = ./modules/consul-connect.nix;

      # TODO PR systemd wants+after "network-online.target"
      # https://groups.google.com/g/consul-tool/c/7fdWDyo9Sws/m/1ObkZS3fGgAJ
      consul = { modulesPath, ... }: {
        disabledModules = [ "services/networking/consul.nix" ];
        imports = [ ./modules/consul.nix ];
      };
      vault = { modulesPath, ... }: {
        disabledModules = [ "services/security/vault.nix" ];
        imports = [ ./modules/vault.nix ];
      };
    };

    nixosProfiles = {
      dev-node-minimal = import ./profiles/dev-node-minimal.nix;
      dev-node-minimal-raid = import ./profiles/dev-node-minimal-raid.nix;
      prod-node2-minimal = import ./profiles/prod-node2-minimal.nix;
      common = import ./profiles/common.nix;
      agent = import ./profiles/agent.nix;
      master = import ./profiles/master.nix;
      master-tmp = import ./profiles/master-tmp.nix;
    };

    nixosConfigurations = {
      dev-node-minimal = lib.makeOverridable lib.nixosSystem {
        inherit system;
        specialArgs = attrs;
        modules = [
          (self.nixosProfiles.dev-node-minimal { inherit authorized-keys state-version; })
        ];
      };

      dev-node1 = lib.makeOverridable lib.nixosSystem {
        inherit system;
        specialArgs = attrs;
        modules = [
          (self.nixosProfiles.dev-node-minimal { inherit authorized-keys state-version; })
          self.nixosProfiles.common
          (self.nixosProfiles.master { name = "node1"; ip = ip-dev-node1; stage = "dev"; })
        ];
      };

      dev-node2 = lib.makeOverridable lib.nixosSystem {
        inherit system;
        specialArgs = attrs;
        modules = [
          (self.nixosProfiles.dev-node-minimal { inherit authorized-keys state-version; })
          self.nixosProfiles.common
          (self.nixosProfiles.agent { name = "node2"; ip = ip-dev-node2; ip-server = ip-dev-node1; stage = "dev"; })
        ];
      };

#      prod-node1-minimal = lib.makeOverridable lib.nixosSystem {
#        inherit system;
#        specialArgs = attrs;
#        modules = [
#          (self.nixosProfiles.prod-node1-minimal { inherit authorized-keys state-version; })
#        ];
#      };

      prod-node2-minimal = lib.makeOverridable lib.nixosSystem {
        inherit system;
        specialArgs = attrs;
        modules = [
          (self.nixosProfiles.prod-node2-minimal { inherit authorized-keys state-version; })
        ];
      };

#      prod-node1 = lib.makeOverridable lib.nixosSystem {
#        inherit system;
#        specialArgs = attrs;
#        modules = [
#          (self.nixosProfiles.prod-node1-minimal { inherit authorized-keys state-version; })
#          self.nixosProfiles.common
#          (self.nixosProfiles.agent { name = "node1"; ip = ip-prod-node1; })
#        ];
#      };

      prod-node2 = lib.makeOverridable lib.nixosSystem {
        inherit system;
        specialArgs = attrs;
        modules = [
          (self.nixosProfiles.prod-node2-minimal { inherit authorized-keys state-version; })
          self.nixosProfiles.common
          (self.nixosProfiles.master-tmp { name = "node2"; ip = ip-prod-node2; stage = "prod"; inherit dns-email; })
#          (self.nixosProfiles.agent { name = "node2"; ip = ip-prod-node2; ip-server = ip-prod-node1; stage = "prod"; })
        ];
      };
    };

    deploy = {
      autoRollback = true;
      magicRollback = true;
      fastConnection = true;
      nodes =
      let
        deploy-dev-node = ip: config: {
          hostname = ip;
          remoteBuild = false;
          profiles.system = {
            sshOpts = [ "-o" "StrictHostKeyChecking=no" ];
            sshUser = "root";
            user = "root";
            path = deploy-rs.lib.x86_64-linux.activate.nixos config;
          };
        };

        deploy-prod-node = ip: config: {
          hostname = ip;
          remoteBuild = true;
          profiles.system = {
            sshUser = "root";
            user = "root";
            path = deploy-rs.lib.x86_64-linux.activate.nixos config;
          };
        };

      in {
        dev-node1 = deploy-dev-node ip-dev-node1 self.nixosConfigurations.dev-node1;
        dev-node2 = deploy-dev-node ip-dev-node2 self.nixosConfigurations.dev-node2;
        prod-test = deploy-prod-node ip-dev-node1 self.nixosConfigurations.dev-node1;
#        prod-node1 = deploy-prod-node ip-prod-node1 self.nixosConfigurations.prod-node1;
        prod-node2 = deploy-prod-node ip-prod-node2 self.nixosConfigurations.prod-node2;
      };
    };

    packages.${system} = {
      # Build that before running gc.
      save-from-gc = pkgs.linkFarm "nofreedisk-save-from-gc" (
        [
          { name = "nixpkgs"; path = nixpkgs; }
          { name = "nixpkgs-unstable"; path = nixpkgs-unstable; }
        ]
        ++ (lib.attrsets.mapAttrsToList (n: v: {
          name = "nixosConfigurations.${n}";
          path = v.config.system.build.toplevel;
        }) self.nixosConfigurations)
        ++ (lib.attrsets.mapAttrsToList (n: path: {
          name = "devShells.${n}";
          inherit path;
        }) (lib.attrsets.filterAttrs (n: v: n == "default") self.devShells.${system}))
      );

      default = self.packages.${system}.help;
      help =
      let
        y = "\\033[01;33m";
        b = "\\033[01m";
        d = "\\033[00m";
          ################################################################# 80 is here ###
        help = ''

          ${y}Nofreedisk NixOS cluster${d}


            Each of the following targets can be accessed two different ways:

            nofreedisk-<target>                 Only accessible from inside dev shells.
                                                Requires to re enter the dev shell to take
                                                into account any target modification.

            nix run .#<target>                  Accessible from both inside and outside
                                                dev shells.

            In which <node> could be either one of:
              dev-node1
              dev-node2
              prod-node1
              prod-node2

            And in which <cluster> could be either one of:
              dev
              prod


            bootstrap-<node>                    Bootstrap node by repartitionning disks
                                                and replacing its random OS by a minimal
                                                NixOS config, to be updated later.

            deploy-<node>                       Update NixOS config on node.

            ssh-<node>                          Connect to node through SSH.

            terraform-apply-<cluster>           Deploy applicative layer into Hashicorp
                                                cluster.

            terraform-destroy-<cluster>         Destroy applicative layer from Hashicorp
                                                cluster.

            help                                Print this help.

            rotate-age-admin-key                Update admin private sops key, update .sops.yaml
                                                and re-encrypt secrets. Pass --help for further
                                                information.

            create-vanilla-secrets              Generates new secrets from a template.
                                                Requires manual deletion of old secrets first,
                                                you may also want to edit .sops.yaml first.

            update-secrets                      Re-encrypt secrets after a .sops.yaml update.

            sendkey-sops-<node>                 Send private sops key to node. Only available
                                                for dev nodes as their private keys are stored
                                                as regular secrets.

            keygen-sops-<node>                  Update private sops key on node, update
                                                .sops.yaml and re-encrypt secrets. Must be
                                                followed by deploy to become effective on node.

            rotate-consul-gossip-key-<cluster>  Update Consul gossip encryption key. Must
                                                be followed by deploy on the dedicated Consul
                                                server node to become effective on cluster.

            rotate-consul-acl-initial-management-<cluster>
                                                Update Consul initial management ACL token.
                                                Must be followed by deploy on the dedicated
                                                Consul leader server node before the ACL system
                                                get bootstrapped to become effective on cluster.

            rotate-consul-tls-ca-<cluster>      Update Consul TLS Certificate Authority. Must be
                                                followed by rotate-consul-tls-certs on the cluster
                                                and deploy on all the nodes to become effective on
                                                the cluster.

            rotate-consul-tls-certs-<cluster>   Update Consul TLS Certificates. Must be followed
                                                by deploy on all the nodes to become effective on
                                                the cluster.

        '';
      in lib.make-task "help" "printf '${help}'";

      status-dev = lib.make-task "status-dev" ''
        virsh list --all | grep -E '^ Id|^----|nofreedisk' --color=never
        echo
        for i in $(virsh list --name | grep -E 'nofreedisk')
        do
          virsh domifaddr $i
        done
      '';

      flake-root = lib.make-task "flake-root" ''
        echo "Before: ''${FLAKE_ROOT}"
        ${lib.flake-root}
        echo "After: ''${FLAKE_ROOT}"
      '';

      create-vanilla-secrets = lib.create-vanilla-secrets {};
      update-secrets = lib.update-secrets {};
      rotate-age-admin-key = lib.rotate-age-admin-key {};

      bootstrap-dev-node1 = lib.bootstrap-dev { task-name = "bootstrap-dev-node1"; ip = ip-dev-node1; config = "dev-node-minimal"; };
      bootstrap-dev-node2 = lib.bootstrap-dev { task-name = "bootstrap-dev-node2"; ip = ip-dev-node2; config = "dev-node-minimal"; };
      bootstrap-prod-test = lib.bootstrap-prod { task-name = "bootstrap-prod-test"; ip = ip-dev-node1; config = "dev-node-minimal"; user = "vagrant"; };
#      bootstrap-prod-node1 = lib.bootstrap-prod { task-name = "bootstrap-prod-node1"; ip = ip-prod-node1; config = "prod-node1-minimal"; user = "debian"; };
      bootstrap-prod-node2 = lib.bootstrap-prod { task-name = "bootstrap-prod-node2"; ip = ip-prod-node2; config = "prod-node2-minimal"; user = "ubuntu"; };

      deploy-dev-node1 = lib.deploy { task-name = "deploy-dev-node1"; config = "dev-node1"; };
      deploy-dev-node2 = lib.deploy { task-name = "deploy-dev-node2"; config = "dev-node2"; };
      deploy-prod-test = lib.deploy { task-name = "deploy-prod-test"; config = "prod-test"; };
#      deploy-prod-node1 = lib.deploy { task-name = "deploy-prod-node1"; config = "prod-node1"; };
      deploy-prod-node2 = lib.deploy { task-name = "deploy-prod-node2"; config = "prod-node2"; };

      ssh-dev-node1 = lib.ssh { task-name = "ssh-dev-node1"; ip = ip-dev-node1; };
      ssh-dev-node2 = lib.ssh { task-name = "ssh-dev-node2"; ip = ip-dev-node2; };
#      ssh-prod-node1 = lib.ssh { task-name = "ssh-prod-node1"; ip = ip-prod-node1; };
      ssh-prod-node2 = lib.ssh { task-name = "ssh-prod-node2"; ip = ip-prod-node2; };

      sendkey-sops-dev-node1 = lib.sendkey-sops-dev { task-name = "sendkey-sops-dev-node1"; ip = ip-dev-node1; file = "secrets/sops-private-dev-node1"; };
      sendkey-sops-dev-node2 = lib.sendkey-sops-dev { task-name = "sendkey-sops-dev-node2"; ip = ip-dev-node2; file = "secrets/sops-private-dev-node2"; };
      keygen-sops-dev-node1 = lib.keygen-sops-dev { task-name = "keygen-sops-dev-node1"; ip = ip-dev-node1; key = "sops-private-dev-node1"; file = "secrets/sops-private-dev-node1"; };
      keygen-sops-dev-node2 = lib.keygen-sops-dev { task-name = "keygen-sops-dev-node2"; ip = ip-dev-node2; key = "sops-private-dev-node2"; file = "secrets/sops-private-dev-node2"; };
      keygen-sops-prod-test = lib.keygen-sops-prod { task-name = "keygen-sops-prod-test"; ip = ip-dev-node1; key = "sops-private-dev-node1"; user = "root"; };
#      keygen-sops-prod-node1 = lib.keygen-sops-prod { task-name = "keygen-sops-prod-node1"; ip = ip-prod-node1; key = "sops-private-prod-node1"; user = "root"; };
      keygen-sops-prod-node2 = lib.keygen-sops-prod { task-name = "keygen-sops-prod-node2"; ip = ip-prod-node2; key = "sops-private-prod-node2"; user = "root"; };

      rotate-consul-gossip-key-dev = lib.rotate-consul-gossip-key { task-name = "rotate-consul-gossip-key-dev"; file = "secrets/dev/consul-encrypt.json"; };
      rotate-consul-gossip-key-prod = lib.rotate-consul-gossip-key { task-name = "rotate-consul-gossip-key-prod"; file = "secrets/prod/consul-encrypt.json"; };
      rotate-consul-acl-initial-management-dev = lib.rotate-consul-acl-initial-management { task-name = "rotate-consul-acl-initial-management-dev"; file = "secrets/dev/consul-acl-initial-management.json"; };
      rotate-consul-acl-initial-management-prod = lib.rotate-consul-acl-initial-management { task-name = "rotate-consul-acl-initial-management-prod"; file = "secrets/prod/consul-acl-initial-management.json"; };
      rotate-consul-tls-ca-dev = lib.rotate-consul-tls-ca { task-name = "rotate-consul-tls-ca-dev"; dest = "secrets/dev"; };
      rotate-consul-tls-ca-prod = lib.rotate-consul-tls-ca { task-name = "rotate-consul-tls-ca-prod"; dest = "secrets/prod"; };
      rotate-consul-tls-certs-dev = lib.rotate-consul-tls-certs {
        task-name = "rotate-consul-tls-certs-dev";
        ca = "secrets/dev/consul-agent-ca.pem";
        key = "secrets/dev/consul-agent-ca-key.pem";
        dest = "secrets/dev";
        certs = [
          {
            kind = "server";
            args = [ "-additional-ipaddress=${ip-dev-node1}" ];
          }
          {
            kind = "client";
            args = [ "-additional-ipaddress=${ip-dev-node2}" ];
          }
        ];
      };
      rotate-consul-tls-certs-prod = lib.rotate-consul-tls-certs {
        task-name = "rotate-consul-tls-certs-prod";
        ca = "secrets/prod/consul-agent-ca.pem";
        key = "secrets/prod/consul-agent-ca-key.pem";
        dest = "secrets/prod";
        certs = [
          {
            kind = "server";
            args = [ "-additional-ipaddress=${ip-prod-node2}" ];
          }
        ];
      };

    }
    // lib.terraform {
      name = "dev";
      auto-approve = true;
      chdir = "terraform";
      backend-secret-config = ./secrets/terraform-backend-dev.json;
      backend-direct-config = {
        CONSUL_HTTP_ADDR = "https://${ip-dev-node1}:8501";
      };
      access-secret-files = {
        CONSUL_CACERT = "secrets/dev/consul-agent-ca.pem";
        CONSUL_CLIENT_CERT = "secrets/dev/dc1-server-consul-0.pem";
        CONSUL_CLIENT_KEY = "secrets/dev/dc1-server-consul-0-key.pem";
        NOMAD_CACERT = "secrets/dev/consul-agent-ca.pem";
        NOMAD_CLIENT_CERT = "secrets/dev/dc1-server-consul-0.pem";
        NOMAD_CLIENT_KEY = "secrets/dev/dc1-server-consul-0-key.pem";
      };
      vars = {
        sops_file = ./secrets/terraform.json;
#        # TODO use '-additional-ipaddress' to add ip-dev-node1 while generating
#        # the cert used by Terraform.
        consul_address = "https://${ip-dev-node1}:8501";
        nomad_address = "https://${ip-dev-node1}:4646";
        domain = "nofreedisk.test";
      };
    }
    // lib.terraform {
      name = "prod";
      auto-approve = true;
      chdir = "terraform";
      backend-secret-config = ./secrets/terraform-backend-prod.json;
      backend-direct-config = {
        CONSUL_HTTP_ADDR = "https://${ip-prod-node2}:8501";
      };
      access-secret-files = {
        CONSUL_CACERT = "secrets/prod/consul-agent-ca.pem";
        CONSUL_CLIENT_CERT = "secrets/prod/dc1-server-consul-0.pem";
        CONSUL_CLIENT_KEY = "secrets/prod/dc1-server-consul-0-key.pem";
        NOMAD_CACERT = "secrets/prod/consul-agent-ca.pem";
        NOMAD_CLIENT_CERT = "secrets/prod/dc1-server-consul-0.pem";
        NOMAD_CLIENT_KEY = "secrets/prod/dc1-server-consul-0-key.pem";
      };
      vars = {
        sops_file = ./secrets/terraform.json;
        consul_address = "https://${ip-prod-node2}:8501";
        nomad_address = "https://${ip-prod-node2}:4646";
        domain = "nofreedisk.fr.eu.org";
      };
    }
    // lib.vagrant {
    }
    // lib.media {
      name = "dev";
      ip = ip-dev-node1;
      user = "root";
      media-dir-var = "NOFREEDISK_MEDIA_DIR_DEV";
      args-var = "NOFREEDISK_MEDIA_ARGS_DEV";
    }
    // lib.media {
      name = "prod";
      ip = ip-prod-node2;
      user = "root";
      media-dir-var = "NOFREEDISK_MEDIA_DIR_PROD";
      args-var = "NOFREEDISK_MEDIA_ARGS_PROD";
      args = [
        "--exclude=staging/"
        "--exclude=musics/.dl*"
      ];
    };

    devShells.${system} = {
      default = pkgs.mkShellNoCC {
        packages = [
          deploy-rs.packages.${system}.deploy-rs
          pkgs.age
          pkgs.sops
          pkgs.vagrant
          pkgs.statix
          pkgs.nomad
          terraform
          self.packages.${system}.help
          self.packages.${system}.flake-root
          self.packages.${system}.create-vanilla-secrets
          self.packages.${system}.update-secrets
          self.packages.${system}.rotate-age-admin-key
          self.packages.${system}.status-dev
          self.packages.${system}.bootstrap-dev-node1
          self.packages.${system}.bootstrap-dev-node2
          self.packages.${system}.bootstrap-prod-test
#          self.packages.${system}.bootstrap-prod-node1
          self.packages.${system}.bootstrap-prod-node2
          self.packages.${system}.deploy-dev-node1
          self.packages.${system}.deploy-dev-node2
          self.packages.${system}.deploy-prod-test
#          self.packages.${system}.deploy-prod-node1
          self.packages.${system}.deploy-prod-node2
          self.packages.${system}.sendkey-sops-dev-node1
          self.packages.${system}.sendkey-sops-dev-node2
          self.packages.${system}.keygen-sops-dev-node1
          self.packages.${system}.keygen-sops-dev-node2
          self.packages.${system}.keygen-sops-prod-test
#          self.packages.${system}.keygen-sops-prod-node1
          self.packages.${system}.keygen-sops-prod-node2
          self.packages.${system}.ssh-dev-node1
          self.packages.${system}.ssh-dev-node2
#          self.packages.${system}.ssh-prod-node1
          self.packages.${system}.ssh-prod-node2
          self.packages.${system}.terraform-apply-dev
          self.packages.${system}.terraform-apply-prod
          self.packages.${system}.terraform-destroy-dev
          self.packages.${system}.terraform-destroy-prod
          self.packages.${system}.vagrant-up-dev
          self.packages.${system}.vagrant-halt-dev
          self.packages.${system}.vagrant-reload-dev
          self.packages.${system}.vagrant-destroy-dev
          self.packages.${system}.rotate-consul-gossip-key-dev
          self.packages.${system}.rotate-consul-gossip-key-prod
          self.packages.${system}.rotate-consul-acl-initial-management-dev
          self.packages.${system}.rotate-consul-acl-initial-management-prod
          self.packages.${system}.rotate-consul-tls-ca-dev
          self.packages.${system}.rotate-consul-tls-ca-prod
          self.packages.${system}.rotate-consul-tls-certs-dev
          self.packages.${system}.rotate-consul-tls-certs-prod
        ] ++ lib.attrsets.attrValues (lib.attrsets.filterAttrs (n: v: lib.strings.hasPrefix "media-" n) self.packages.${system});

        shellHook = ''
          ${lib.flake-root}
          ${lib.sops-age-key-file}

          export NOFREEDISK_MEDIA_DIR_DEV="''${FLAKE_ROOT}/media"
          if [ ! -v NOFREEDISK_MEDIA_DIR_PROD ]
          then
            export NOFREEDISK_MEDIA_DIR_PROD="''${FLAKE_ROOT}/media"
          fi
        '';
      };

      icon = pkgs.mkShellNoCC {
        packages = [
          pkgs.go-task
          pkgs.pdf2svg
          pkgs.python310Packages.cairosvg
          (pkgs.texlive.combine {
            inherit (pkgs.texlive) scheme-small standalone pgfplots;
          })
        ];
      };
      media = pkgs.mkShellNoCC {
        packages = lib.attrsets.attrValues (lib.attrsets.filterAttrs (n: v: lib.strings.hasPrefix "media-" n) self.packages.${system});
      };
    };
  };
}
