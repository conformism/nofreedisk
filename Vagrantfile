additional_disks = ENV[ "ADDITIONAL_DISKS" ]
nodes_number = 2
box = "generic/debian11"
#box = "generic/alpine317"
#box = "jcmdln/nixos"
ip_offset = 10
ip_lan = "192.168.240."
mac_common = "52:54:00:00:25:"
prefix = "nofreedisk_"
network = "#{prefix}net"

Vagrant.configure(2) do |config|
  config.vagrant.plugins = [ "vagrant-libvirt" ]

  config.vm.provider :libvirt do |libvirt|
    libvirt.cpus = 3
    libvirt.memory = 4096
    libvirt.driver = "kvm"
    libvirt.default_prefix = "#{prefix}"
    (1..additional_disks.to_i).each do |i|
      # First disk' size cannot be reduced, so let's grow this one the same size.
      libvirt.storage :file, :size => 128
    end
  end

  config.ssh.password = "vagrant";

  (1..nodes_number).each do |i|
    config.vm.define "node#{i}" do |config|
      config.vm.hostname = "node#{i}"
      config.vm.box = box
      config.vm.network :private_network,
        # For interface name predictability : ens20 (won't be enp0s20 in first place)
        :bus => 0,
        :slot => 20,
        :ip => "#{ip_lan}#{i+ip_offset}",
        :mac => "#{mac_common}#{i+ip_offset}",
        :libvirt__netmask => "255.255.255.0",
        :libvirt__network_name => "#{network}",
        :libvirt__dhcp_enabled => true

#      # Alpine things (kexec should be available in 3.18)
#      config.vm.provision "shell", :inline => "
#        sed -i /etc/apk/repositories -e 's/v3.17/edge/g'
#        apk upgrade --available --update-cache
#        apk add kexec-tools --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing
#        passwd -u root
#      "
    end

    config.trigger.after :up do |trigger|
      trigger.name = "Force IP adresses in Libvirt DHCP server"
      trigger.only_on = "node#{i}"
      trigger.on_error = :continue
      trigger.run = {:inline => "bash -c '
        Q=\\\"
        if [[ \"$(virsh net-dumpxml #{network} | grep #{mac_common}#{i+ip_offset} )\" ]]
        then
          virsh net-update \
            --network #{network} \
            --command delete \
            --section ip-dhcp-host \
            --xml \"<host mac=${Q}#{mac_common}#{i+ip_offset}${Q}/>\" \
            --live
        fi
        virsh net-update \
          --network #{network} \
          --command add \
          --section ip-dhcp-host \
          --xml \"<host mac=${Q}#{mac_common}#{i+ip_offset}${Q} name=${Q}#{prefix}node#{i}${Q} ip=${Q}#{ip_lan}#{i+ip_offset}${Q}/>\" \
          --live
      '"}
    end
  end
end
