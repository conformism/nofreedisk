# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com), and this project adheres to [Romantic Versioning](https://github.com/romversioning/romver).

## [Unreleased]

## [2.0.0] - 1970-01-01

## [1.0.0] - 1970-01-01
