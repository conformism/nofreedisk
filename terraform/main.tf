terraform {
  required_version = ">= 0.13"
  required_providers {
    consul = {
      source = "hashicorp/consul"
    }
    nomad = {
      source = "hashicorp/nomad"
    }
    sops = {
      source = "carlpett/sops"
    }
    vault = {
      source = "hashicorp/vault"
    }
  }

  backend "consul" {
    scheme = "https"
    path = "terraform/tfstate"
  }
}
