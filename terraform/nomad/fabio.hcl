job "fabio" {
  datacenters = [ "dc1" ]
  type = "system"

  group "fabio" {
    network {
      port "lb" {
        static = 9999
      }
      port "ui" {
        static = 9998
      }
    }

#    volume "fabio-properties" {
#      type      = "host"
#      read_only = true
#      source    = "fabio-properties"
#    }

    task "fabio" {
      driver = "docker"
      config {
        image = "fabiolb/fabio"
        network_mode = "host"
        ports = [ "lb", "ui" ]
        args = [
          "-proxy.strategy=rr",
          "-registry.consul.addr=192.168.240.11:8500"
        ]
#        volumes = [ "/etc/fabio/fabio.properties:/etc/fabio/fabio.properties" ]
      }

      resources {
        cpu    = 200
        memory = 128
      }

#      volume_mount {
#        volume      = "fabio-properties"
#        destination = "/etc/fabio"
#        read_only   = true
#      }

#      restart {
#        attempts = 10
#        interval = "10s"
#      }
    }
  }
}
