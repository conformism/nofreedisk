resource "consul_config_entry" "route-api-to-consul-ui" {
  kind = "http-route"
  name = "api-to-consul-ui"
  depends_on = [ consul_config_entry.defaults-consul-ui ]
  config_json = jsonencode({
    Hostnames = [ "consul.${var.domain}" ]
    Parents = [
      {
        Kind = "api-gateway"
        Name = "api"
      }
    ]
    Rules = [
      {
        Services = [
          {
            Name = "consul-ui"
          }
        ]
      }
    ]
  })
}

resource "consul_config_entry" "intentions-consul-ui" {
  kind = "service-intentions"
  name = "consul-ui"
  depends_on = [ consul_config_entry.defaults-consul-ui ]
  config_json = jsonencode({
    Sources = [
      {
        Name = "ingress"
        Action = "allow"
      },
      {
        Name = "api"
        Action = "allow"
      }
    ]
  })
}

resource "consul_config_entry" "defaults-consul-ui" {
  kind = "service-defaults"
  name = "consul-ui"
  config_json = jsonencode({
    Protocol = "http"
  })
}
