resource "consul_config_entry" "resolver-color" {
  kind = "service-resolver"
  name = "color"
  depends_on = [ consul_config_entry.defaults-color ]
  config_json = jsonencode({
    Subsets = {
      "green" = {
        Filter = "Service.Tags contains green"
        OnlyPassing = true
      }
      "blue" = {
        Filter = "Service.Tags contains blue"
        OnlyPassing = true
      }
      "yellow" = {
        Filter = "Service.Tags contains yellow"
        OnlyPassing = true
      }
    }
  })
}

resource "consul_config_entry" "splitter-color" {
  kind = "service-splitter"
  name = "color"
  depends_on = [
    consul_config_entry.defaults-color,
    consul_config_entry.resolver-color
  ]
  config_json = jsonencode({
    Splits = [
      {
        Weight = 70
        ServiceSubset = "green"
      },
      {
        Weight = 10
        ServiceSubset = "blue"
      },
      {
        Weight = 20
        ServiceSubset = "yellow"
      }
    ]
  })
}

resource "consul_config_entry" "route-api-to-color" {
  kind = "http-route"
  name = "api-to-color"
  depends_on = [ consul_config_entry.defaults-color ]
  config_json = jsonencode({
    Hostnames = [ "color.${var.domain}" ]
    Parents = [
      {
        Kind = "api-gateway"
        Name = "api"
      }
    ]
    Rules = [
      {
        Services = [
          {
            Name = "color"
          }
        ]
      }
    ]
  })
}

resource "consul_config_entry" "intentions-color" {
  kind = "service-intentions"
  name = "color"
  depends_on = [ consul_config_entry.defaults-color ]
  config_json = jsonencode({
    Sources = [
      {
        Name = "ingress"
        Action = "allow"
#        Permissions = [
#          {
##            Name = ""
#            Action = "allow"
#            HTTP = {
#              PathPrefix = "/"
#              Methods = ["GET", "PUT", "POST", "DELETE", "HEAD"]
#            }
#          }
#        ]
      },
      {
        Name = "api"
        Action = "allow"
      }
    ]
  })
}

resource "consul_config_entry" "defaults-color" {
  kind = "service-defaults"
  name = "color"
  config_json = jsonencode({
    Protocol = "http"
  })
}
