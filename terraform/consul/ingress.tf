# TODO Switch to API gateway
# https://developer.hashicorp.com/consul/docs/connect/config-entries/api-gateway

resource "consul_config_entry" "ingress" {
  kind = "ingress-gateway"
  name = "ingress"
  depends_on = [
    # These dependencies are important !
    consul_config_entry.defaults-jellyfin,
    consul_config_entry.defaults-2048-game,
    consul_config_entry.defaults-color,
    consul_config_entry.defaults-consul-ui
  ]
  config_json = jsonencode({
    TLS = {
      Enabled = true # Defaults to built in TLS CA
#      # TODO use certs from NixOS ACME module
#      # https://developer.hashicorp.com/consul/docs/connect/gateways/ingress-gateway/tls-external-service
#      # https://developer.hashicorp.com/consul/docs/connect/config-entries/ingress-gateway
#      # https://www.envoyproxy.io/docs/envoy/latest/configuration/security/secret
#      # https://sagikazarmark.hu/blog/manage-certificates-on-nixos-using-acme-and-google-domains/
#      SDS = {
#        ClusterName = "<name of SDS cluster>"
#        CertResource = "<SDS resource name>"
#      }
    }
    Listeners = [
      {
        Port = 4433
#        Protocol = "tcp"
        Protocol = "http"
        Services = [
          {
            Name = "color"
            Hosts = [ "color.${var.domain}" ]
          },
          {
            Name = "jellyfin"
            Hosts = [ "jellyfin.${var.domain}" ]
          },
          {
            Name = "consul-ui"
            Hosts = [ "consul.${var.domain}" ]
          },
          {
            Name = "2048-game"
            Hosts = [ "2048-game.${var.domain}" ]
          },
        ]
      }
    ]
  })
}
