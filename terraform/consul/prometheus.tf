resource "consul_config_entry" "route-api-to-prometheus" {
  kind = "http-route"
  name = "api-to-prometheus"
  depends_on = [ consul_config_entry.defaults-prometheus ]
  config_json = jsonencode({
    Hostnames = [ "prometheus.${var.domain}" ]
    Parents = [
      {
        Kind = "api-gateway"
        Name = "api"
      }
    ]
    Rules = [
      {
        Services = [
          {
            Name = "prometheus"
          }
        ]
      }
    ]
  })
}

resource "consul_config_entry" "intentions-prometheus" {
  kind = "service-intentions"
  name = "prometheus"
  depends_on = [ consul_config_entry.defaults-prometheus ]
  config_json = jsonencode({
    Sources = [
      {
        Name = "ingress"
        Action = "allow"
      },
      {
        Name = "api"
        Action = "allow"
      }
    ]
  })
}

resource "consul_config_entry" "defaults-prometheus" {
  kind = "service-defaults"
  name = "prometheus"
  config_json = jsonencode({
    Protocol = "http"
  })
}
