resource "consul_config_entry" "route-api-to-jellyfin" {
  kind = "http-route"
  name = "api-to-jellyfin"
  depends_on = [ consul_config_entry.defaults-jellyfin ]
  config_json = jsonencode({
    Hostnames = [ "jellyfin.${var.domain}" ]
    Parents = [
      {
        Kind = "api-gateway"
        Name = "api"
      }
    ]
    Rules = [
      {
        Services = [
          {
            Name = "jellyfin"
          }
        ]
      }
    ]
  })
}

resource "consul_config_entry" "intentions-jellyfin" {
  kind = "service-intentions"
  name = "jellyfin"
  depends_on = [ consul_config_entry.defaults-jellyfin ]
  config_json = jsonencode({
    Sources = [
      {
        Name = "ingress"
        Action = "allow"
      },
      {
        Name = "api"
        Action = "allow"
      }
    ]
  })
}

resource "consul_config_entry" "defaults-jellyfin" {
  kind = "service-defaults"
  name = "jellyfin"
  config_json = jsonencode({
    Protocol = "http"
  })
}
