resource "consul_config_entry" "intentions-global" {
  kind = "service-intentions"
  name = "*"
  config_json = jsonencode({
    Sources = [
      {
        Name = "*"
        Action = "deny"
      }
    ]
  })
}
