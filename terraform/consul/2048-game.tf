resource "consul_config_entry" "route-api-to-2048-game" {
  kind = "http-route"
  name = "api-to-2048-game"
  depends_on = [ consul_config_entry.defaults-2048-game ]
  config_json = jsonencode({
    Hostnames = [ "2048-game.${var.domain}" ]
    Parents = [
      {
        Kind = "api-gateway"
        Name = "api"
      }
    ]
    Rules = [
      {
        Services = [
          {
            Name = "2048-game"
          }
        ]
      }
    ]
  })
}

resource "consul_config_entry" "intentions-2048-game" {
  kind = "service-intentions"
  name = "2048-game"
  depends_on = [ consul_config_entry.defaults-2048-game ]
  config_json = jsonencode({
    Sources = [
      {
        Name = "ingress"
        Action = "allow"
      },
      {
        Name = "api"
        Action = "allow"
      }
    ]
  })
}

resource "consul_config_entry" "defaults-2048-game" {
  kind = "service-defaults"
  name = "2048-game"
  config_json = jsonencode({
    Protocol = "http"
  })
}
