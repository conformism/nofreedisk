resource "consul_config_entry" "api" {
  kind = "api-gateway"
  name = "api"
  config_json = jsonencode({
    Listeners = [
      {
        Name = "gate"
        Protocol = "http"
        Port = 443
        TLS = {
          Certificates = {
            Kind = "inline-certificate"
            Name = "nofreedisk"
          }
        }
      }
    ]
  })
}
