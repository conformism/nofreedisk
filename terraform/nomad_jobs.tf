provider "nomad" {
  address = var.nomad_address
}

#resource "nomad_job" "game_2048" {
#  jobspec = file("${path.module}/nomad/2048-game.hcl")
#  consul_token = "${consul_acl_token.nomad-token.id}"
#}

resource "nomad_job" "fabio" {
  jobspec = file("${path.module}/nomad/fabio.hcl")
  consul_token = "${consul_acl_token.nomad-token.id}"
}

resource "nomad_job" "http-echo-gui" {
  jobspec = file("${path.module}/nomad/http-echo.hcl")
  consul_token = "${consul_acl_token.nomad-token.id}"
}

resource "nomad_job" "countdash" {
  jobspec = file("${path.module}/nomad/countdash.hcl")
  consul_token = "${consul_acl_token.nomad-token.id}"
}
