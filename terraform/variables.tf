variable "consul_address" {
  type = string
  nullable = false
}

variable "domain" {
  type = string
  nullable = false
}

variable "nomad_address" {
  type = string
  nullable = false
}

variable "sops_file" {
  type = string
  nullable = false
}
