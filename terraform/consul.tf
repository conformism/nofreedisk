provider "consul" {
  address = var.consul_address
}

module "consul" {
  source = "./consul"
  domain = var.domain
}

resource "consul_config_entry" "intentions-countdash" {
  kind = "service-intentions"
  name = "count-api"
  config_json = jsonencode({
    Sources = [
      {
        Name = "count-dashboard"
        Action = "allow"
      }
    ]
  })
}

resource "consul_acl_policy" "nomad-client" {
  name = "nomad-client"
  description = "Nomad Client Policy"
  rules = file("${path.module}/consul/nomad-client-policy.hcl")
}

resource "consul_acl_policy" "nomad-server" {
  name = "nomad-server"
  description = "Nomad Server Policy"
  rules = file("${path.module}/consul/nomad-server-policy.hcl")
}

resource "consul_acl_token" "nomad-token" {
  description = "Nomad token"
  policies = [
      "${consul_acl_policy.nomad-client.name}",
      "${consul_acl_policy.nomad-server.name}"
    ]
}
