provider "sops" {}

data "sops_file" "secrets" {
  source_file = var.sops_file
}

output "example" {
  value = nonsensitive(data.sops_file.secrets.data["example"])
}
