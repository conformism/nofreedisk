{ self
, lib
, pkgs
, sops ? pkgs.sops
}:

# Decrypt sops files impurely at evaluation time. This is suitable for private
# data but *not* for secret data since it will end up unencrypted in the store.
secrets:

with lib.asserts;
with lib.strings;

let
  decrypt = secret:
    assert assertMsg (builtins.isPath secret)
      ("Secret \"${toString secret}\" should be of type path"
      + optionalString (builtins.isString secret) " (Remove double quotes)"
      + ".");
    rec {
      name = removePrefix (toString self + "/secrets/") (toString secret);
      value = pkgs.runCommand "decrypted-${name}" {
        __noChroot = true;
        preferLocalBuild = true;
        src = secret;
      } ''
        ${lib.sops-age-key-file}
        mkdir -p ''${out}/${removeSuffix (builtins.baseNameOf name) name}
        ${sops}/bin/sops -d ${secret} > ''${out}/${name}
      '' + "/${name}";
    };

in builtins.listToAttrs (map decrypt secrets)
