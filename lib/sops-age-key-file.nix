{ lib
, pkgs
, age ? pkgs.age
, file-basename ? "nofreedisk.txt"
}:

assert lib.asserts.assertMsg ((builtins.baseNameOf file-basename) == file-basename)
  "The Age key file '${file-basename}' must be given just as a filename, not a path.";

# Use this as a guard inside each shell task that aim to access any Sops file.
# It will set SOPS_AGE_KEY_FILE regarding Nix was installed as a daemon or in
# single-user mode.
#
# https://discourse.nixos.org/t/what-is-the-simplest-way-to-check-if-one-has-single-or-multi-user-nix-installed/13882/4
# https://github.com/lilyball/nix-env.fish/blob/master/conf.d/nix-env.fish
# https://github.com/NixOS/nix/issues/3630
# Watch this in case it finally gets implemented in Nix.
''
  if [ ! -v SOPS_AGE_KEY_FILE ]
  then
    if [ -k /nix/store ] && [ $(ls -nd /nix/store | awk '{print $3}') -eq 0 ]
    then
      export SOPS_AGE_KEY_FILE=/etc/sops/age/${file-basename}
    else
      HOME=$(bash -c "realpath ~$(whoami)")
      export SOPS_AGE_KEY_FILE=''${HOME}/.config/sops/age/${file-basename}
    fi
  fi
''
