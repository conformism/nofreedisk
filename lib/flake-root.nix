{ lib
, pkgs
, jq ? pkgs.jq
, debug ? false
}:

assert lib.asserts.assertMsg (builtins.isBool debug)
  "The debug field must be a boolean.";

# Use this as a guard inside each shell task that aim to modify any file inside
# the project's directoy structure.
# Then refer to the actual files relative to ${FLAKE_ROOT} .
# You can also use it inside the shellHook of a devShell : it will avoid
# performing the check multiple times, and thus allow you to run further
# commands from any directory you want.
#
# https://github.com/NixOS/nix/issues/8034
# https://github.com/srid/flake-root
# Watch this in case it finally gets implemented in Nix.
''
  if [ ! -v FLAKE_ROOT ]
  then
    FLAKE_RESOLVED_URL=$(nix flake metadata --no-warn-dirty --quiet --json | ${jq}/bin/jq -r .resolvedUrl)

    if ( ! grep -Eq '^git\+file://' <<< "''${FLAKE_RESOLVED_URL}" )
    then
      echo "ERROR : Be sure to run this command from either inside the" \
        "directory structure of a git clone of the project, either from" \
        "inside the project's devshell"
      ${lib.optionalString debug ''echo "Current flake URL : ''${FLAKE_RESOLVED_URL}"''}
      exit 1
    else
      FLAKE_ROOT=$(sed -e 's/^git+file:\/\///' <<< "''${FLAKE_RESOLVED_URL}")
      export FLAKE_ROOT
      ${lib.optionalString debug ''echo "FLAKE_ROOT=''${FLAKE_ROOT}"''}
    fi
  fi
''
