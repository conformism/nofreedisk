{ lib
, require-tmpfs ? true
}:

assert lib.asserts.assertMsg (builtins.isBool require-tmpfs)
  "The require-tmpfs field must be a boolean.";

# Use this as a guard inside each shell task that aim to temporary decrypt some
# secrets. Secrets will be decrypted in RAM and will be cleaned after usage.
# Using shred should be effective on tmpfs since it is not journaled.
# Disabling require-tmpfs will probably leak secrets on disk at usage, thus it
# is not recomended.
# TODO consider using novops

# http://redsymbol.net/articles/bash-exit-traps/

let
  put-in-ram-if-possible = ''
    TMPDIR=$(mktemp -d $([ -d /dev/shm ] && echo '-p /dev/shm'))
  '';

  put-in-ram-or-abort = ''
    if [ -d /dev/shm ]
    then
      TMPDIR=$(mktemp -d -p /dev/shm)
    else
      echo "ERROR : /dev/shm tmpfs does not exist but is required to create TMPDIR"
      exit 1
    fi
  '';

in (if require-tmpfs then put-in-ram-or-abort else put-in-ram-if-possible) + ''
  function clean {
    find ''${TMPDIR} -depth -type f -exec shred -zu {} \;
    rmdir ''${TMPDIR}
  }
  trap clean EXIT >/dev/null
''
