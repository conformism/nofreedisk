{ lib
, pkgs
, sops ? pkgs.sops
, consul ? pkgs.consul
}:

{ task-name
, key ? ''["encrypt"]''
, file
, config ? ".sops.yaml"
}:

with lib.asserts;
assert assertMsg (builtins.isString file)
  ("The file \"${toString file}\" containing Consul gossip encryption key should "
  + "be given as a string, relative to project's root directory.");

# https://developer.hashicorp.com/consul/docs/agent/config/cli-flags#_encrypt
# https://developer.hashicorp.com/consul/tutorials/security/gossip-encryption-secure
# https://developer.hashicorp.com/consul/tutorials/datacenter-operations/gossip-encryption-rotate

(lib.make-task task-name ''
  ${lib.flake-root}
  ${lib.sops-age-key-file}
  set -e

  if [ ! -f ''${FLAKE_ROOT}/${file} ]
  then
    echo "ERROR : ''${FLAKE_ROOT}/${file} doesn't exist, please create it before"
  else
    KEY='${key}'
    ${sops}/bin/sops \
      --config ''${FLAKE_ROOT}/${config} \
      --set "''${KEY} \"$(consul keygen)\"" \
      ''${FLAKE_ROOT}/${file}
  fi
'')
