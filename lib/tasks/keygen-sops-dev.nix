{ lib
, pkgs
, age ? pkgs.age
, sops ? pkgs.sops
}:

{ task-name
, ip
, key
, file
, destination ? "/var/lib/sops-private-key"
, config ? ".sops.yaml"
}:

with lib.asserts;
assert assertMsg (builtins.isString file)
  ("The file \"${toString file}\" containing the sops private key should be "
  + "given as a string, relative to project's root directory.");

(lib.make-task task-name ''
  ${lib.flake-root}
  ${lib.sops-age-key-file}
  ${lib.tmpdir}
  set -e

  ssh root@${ip} '
    mkdir -p ${destination}'
  rm -f ''${FLAKE_ROOT}/${file}

  # Create age keys locally.
  CLEAR_KEY=''${TMPDIR}/${builtins.baseNameOf file}
  ${age}/bin/age-keygen -o ''${CLEAR_KEY}
  NEW_PUBKEY=$(${age}/bin/age-keygen -y ''${CLEAR_KEY})

  # Update public key in sops config file.
  sed \
    -i ''${FLAKE_ROOT}/${config} \
    -e "s/&${key}.*$/\&${key} ''${NEW_PUBKEY}/"

  # Send private key to node.
  scp \
    ''${CLEAR_KEY} \
    root@${ip}:${destination}/$(date -u +%F-%H-%M-%S)

  # Encrypt and store private key in local sops secret.
  #
  # Avoid clear secrets being written on disk :
  # symlink to destination, so that the symlink matches a sops rule,
  # encrypt in place, then move back to destination, on disk.
  ln -sfT ''${CLEAR_KEY} ''${FLAKE_ROOT}/${file}
  ${sops}/bin/sops \
    --config ''${FLAKE_ROOT}/${config} \
    -e -i ''${FLAKE_ROOT}/${file}
  mv -f ''${CLEAR_KEY} ''${FLAKE_ROOT}/${file}

  ${lib.update-secrets { inherit config; }}/bin/nofreedisk-update-secrets
'')
