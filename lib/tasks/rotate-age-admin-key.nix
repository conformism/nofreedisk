{ lib
, pkgs
, age ? pkgs.age
, sops ? pkgs.sops
}:

{ config ? ".sops.yaml"
}:

(lib.make-task "rotate-age-admin-key" ''
  ${lib.flake-root}
  ${lib.sops-age-key-file}
  ${lib.tmpdir}

  OPTS=$(getopt \
    -o h \
    -l help,gc,force \
    -n "$0" \
    -- "$@") \
    || exit
  eval set -- "''${OPTS}"
  while [[ $1 != -- ]]
  do
    case $1 in
      -h|--help) OPT_HELP='true' ; shift ;;
      --gc) OPT_GC='true' ; shift ;;
      --force) OPT_FORCE='true' ; shift ;;
      *) echo "Bad option: $1" >&2 ; exit 1 ;;
    esac
  done
  shift

  _help () {
    echo -e "\
  Usage :
      -h, --help              Print this help.
      <admin_name>            Rotate the Age key pair, regarding ''${FLAKE_ROOT}/${config}.
      --gc                    Removes old keys. Once key rotated, be sure to have commited
                              changes correctly before removing old keys.
      <admin_name> --force    Force age key rotation, does not update secrets. ACCESS TO
                              CURRENT SECRETS WILL BE LOST. Only suitable to create a new
                              key after complete secret deletion, or to create a new admin's
                              key that must be further validated by another admin by
                              re-encrypting secrets.
  "
  }

  _gc () {
    echo "Remove old keys in ''${SOPS_AGE_KEY_FILE}.bak"
    rm -rfv "''${SOPS_AGE_KEY_FILE}.bak"
  }

  _rotate_force () {
    echo "Rotating Age key for $1"

    OLD_KEYFILE="''${SOPS_AGE_KEY_FILE}"
    NEW_KEYFILE="''${TMPDIR}/$(basename "''${OLD_KEYFILE}")"
    age-keygen > "''${NEW_KEYFILE}"
    NEW_PUBKEY=$(age-keygen -y "''${NEW_KEYFILE}" | tail -1)

    # TODO if name present, replace everything after
    # else add line after '^keys:'
    if grep -qE "$1" "''${FLAKE_ROOT}/${config}"
    then
      sed \
        -i "''${FLAKE_ROOT}/${config}" \
        -e "s/&$1.*$/\&$1 ''${NEW_PUBKEY}/"
    else
      sed \
        -i "''${FLAKE_ROOT}/${config}" \
        -e "/keys:/a \ \ - &$1 ''${NEW_PUBKEY}"
    fi

    if [ -f "''${OLD_KEYFILE}" ]
    then
      mkdir -p "''${OLD_KEYFILE}.bak"
      mv "''${OLD_KEYFILE}" "''${OLD_KEYFILE}.bak/$(date -u +%F-%H-%M-%S)"
      echo -e "\nOld keys saved to ''${OLD_KEYFILE}.bak, do not forget to run --gc after " \
        "having properly commit updated secrets."
    fi
    mv "''${NEW_KEYFILE}" "''${OLD_KEYFILE}"
  }

  _rotate () {
    echo "Rotating Age key for $1"

    OLD_PUBKEY=$(age-keygen -y "''${SOPS_AGE_KEY_FILE}" | tail -1)
    # Check current pubkey and given admin name match together in .sops.yaml
    if ! grep -qE "$1 +''${OLD_PUBKEY}" "''${FLAKE_ROOT}/${config}"
    then
      echo "ERROR : Current key ''${OLD_PUBKEY} cannot be found for $1 in Sops config file ''${FLAKE_ROOT}/${config}"
      exit 1
    fi

    OLD_KEYFILE="''${SOPS_AGE_KEY_FILE}"
    NEW_KEYFILE="''${TMPDIR}/$(basename "''${OLD_KEYFILE}")"
    age-keygen > "''${NEW_KEYFILE}"
    NEW_PUBKEY=$(age-keygen -y "''${NEW_KEYFILE}" | tail -1)

    sed \
      -i "''${FLAKE_ROOT}/${config}" \
      -e "s/&$1.*$/\&$1 ''${NEW_PUBKEY}/"

    if ${lib.update-secrets { inherit config; }}/bin/nofreedisk-update-secrets
    then
      mkdir -p "''${OLD_KEYFILE}.bak"
      mv "''${OLD_KEYFILE}" "''${OLD_KEYFILE}.bak/$(date -u +%F-%H-%M-%S)"
      mv "''${NEW_KEYFILE}" "''${OLD_KEYFILE}"
      echo -e "\nOld keys saved to ''${OLD_KEYFILE}.bak, do not forget to run --gc after " \
        "having properly commit updated secrets."
    else
      echo "ERROR : Failed to update secrets, cannot rotate Age admin keys"
      exit 1
    fi
  }

  if [[ "''${OPT_HELP}" ]]
  then
    _help
  elif [[ "$#" = 0 && ! "''${OPT_FORCE}" && "''${OPT_GC}" ]]
  then
    _gc
  elif [[ "$#" = 1 && "''${OPT_FORCE}" && ! "''${OPT_GC}" ]]
  then
    _rotate_force "$1"
  elif [[ "$#" = 1 && ! "''${OPT_FORCE}" && ! "''${OPT_GC}" ]]
  then
    _rotate "$1"
  else
    _help
    exit 1
  fi

  exit 0
'')
