{ lib
, pkgs
, nixos-images
, nixos-anywhere
, sshpass ? pkgs.sshpass
}:

{ task-name
, ip
, config
}:

#    --no-reboot \
(lib.make-task task-name ''
  ${lib.flake-root}
  ssh-keygen -f "''${HOME}/.ssh/known_hosts" -R ${ip}
  ${sshpass}/bin/sshpass -p vagrant \
    ssh-copy-id -o StrictHostKeyChecking=no vagrant@${ip}
  ssh vagrant@${ip} 'sudo cp -r ~/.ssh /root/'
  ${nixos-anywhere}/bin/nixos-anywhere \
    --flake ''${FLAKE_ROOT}#${config} \
    --kexec "${nixos-images}/nixos-kexec-installer-x86_64-linux.tar.gz" \
    --no-substitute-on-destination \
    root@${ip}
  ssh-keygen -f "''${HOME}/.ssh/known_hosts" -R ${ip}
  ssh -o StrictHostKeyChecking=no root@${ip} true
'')
