{ lib
, pkgs
, sops ? pkgs.sops
}:

{ config ? ".sops.yaml"
}:

lib.make-task "create-vanilla-secrets" ''
  ${lib.flake-root}
  ${lib.sops-age-key-file}
  if [ -d ''${FLAKE_ROOT}/secrets ]
  then
    echo "ERROR : ''${FLAKE_ROOT}/secrets already exist, remove it first"
  else
    cp -r \
      ''${FLAKE_ROOT}/templates/secrets \
      ''${FLAKE_ROOT}/secrets
    for secret in $(find ''${FLAKE_ROOT}/secrets -type f)
    do
      ${sops}/bin/sops -e -i \
        --config ''${FLAKE_ROOT}/${config} \
        ''${secret}
    done
  fi
''
