{ lib
, pkgs
, sops ? pkgs.sops
, terraform ? pkgs.terraform
}:

{ name
, chdir ? "."
, vars ? null
, access-secret-files ? {}
, backend-direct-config ? {}
, backend-secret-config ? null
, auto-approve ? false
, config ? ".sops.yaml"
}:

with lib.asserts;
assert assertMsg (builtins.isString chdir)
  ("The chdir directory \"${toString chdir}\" from which Terraform will be run"
  + "should be given as a string, relative to project's root directory.");

let
  arg-auto-approve = lib.optionalString auto-approve "-auto-approve";

  arg-vars =
  let
    fix-paths-attrs = key: value:
      if (lib.isList value)
      then (map fix-paths-list value)
      else toString value;
    fix-paths-list = element:
      if (lib.isAttrs element)
      then (lib.mapAttrsRecursive fix-paths-attrs element)
      else toString element;
    make-tfvars = name: vars:
      (builtins.toFile "${name}.tfvars.json"
      (builtins.toJSON (lib.mapAttrsRecursive fix-paths-attrs vars)));
  in lib.optionalString (vars != null)
    "-var-file=\"${make-tfvars name vars}\"";

  export-secret-config = lib.optionalString (backend-secret-config != null) ''
    DECRYPTED="$(${sops}/bin/sops \
      --config ''${FLAKE_ROOT}/${config} \
      --output-type dotenv \
      -d ${toString backend-secret-config})"
    export ''${DECRYPTED}
  '';

  export-direct-config = lib.concatLines (lib.mapAttrsToList
    (name: value:
      "export ${name}=\"${toString value}\""
    ) backend-direct-config);

  decrypt-secret-files = lib.optionalString (access-secret-files != {}) (''
      ${lib.tmpdir}

      for file in ${toString (lib.mapAttrsToList (name: file: toString file) access-secret-files)}
      do
        ${sops}/bin/sops \
          --config ''${FLAKE_ROOT}/${config} \
          --output ''${TMPDIR}/$(basename ''${file}) \
          -d ''${FLAKE_ROOT}/''${file}
      done
    ''
    + lib.concatLines (lib.mapAttrsToList
    (name: file:
      ''export ${name}="''${TMPDIR}/${builtins.baseNameOf file}"''
    ) access-secret-files));

in {
  "terraform-apply-${name}" = (lib.make-task "terraform-apply-${name}" ''
    ${lib.flake-root}
    ${lib.sops-age-key-file}
    set -e
    ${export-secret-config}
    ${export-direct-config}
    ${decrypt-secret-files}
    rm -rf ''${FLAKE_ROOT}/${chdir}/.terraform
    rm -rf ''${FLAKE_ROOT}/${chdir}/.terraform.lock.hcl
    ${terraform}/bin/terraform -chdir=''${FLAKE_ROOT}/${chdir} init ${arg-vars} -reconfigure
    ${terraform}/bin/terraform -chdir=''${FLAKE_ROOT}/${chdir} apply ${arg-vars} ${arg-auto-approve}
  '');

  "terraform-destroy-${name}" = (lib.make-task "terraform-destroy-${name}" ''
    ${lib.flake-root}
    ${lib.sops-age-key-file}
    set -e
    ${export-secret-config}
    ${export-direct-config}
    ${decrypt-secret-files}
    rm -rf ''${FLAKE_ROOT}/${chdir}/.terraform
    rm -rf ''${FLAKE_ROOT}/${chdir}/.terraform.lock.hcl
    ${terraform}/bin/terraform -chdir=''${FLAKE_ROOT}/${chdir} init ${arg-vars} -reconfigure
    ${terraform}/bin/terraform -chdir=''${FLAKE_ROOT}/${chdir} destroy ${arg-vars} ${arg-auto-approve}
  '');
}
