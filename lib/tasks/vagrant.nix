{ lib
, pkgs
, vagrant ? pkgs.vagrant
}:

{ name ? "dev"
}:

{
  "vagrant-up-${name}" = (lib.make-task "vagrant-up-${name}" ''
    ${lib.flake-root}
    set -e
    cd ''${FLAKE_ROOT}
    ${vagrant}/bin/vagrant up "$@"
  '');

  "vagrant-halt-${name}" = (lib.make-task "vagrant-halt-${name}" ''
    ${lib.flake-root}
    set -e
    cd ''${FLAKE_ROOT}
    ${vagrant}/bin/vagrant halt "$@"
  '');

  "vagrant-destroy-${name}" = (lib.make-task "vagrant-destroy-${name}" ''
    ${lib.flake-root}
    set -e
    cd ''${FLAKE_ROOT}
    ${vagrant}/bin/vagrant destroy "$@"
  '');

  "vagrant-reload-${name}" = (lib.make-task "vagrant-reload-${name}" ''
    ${lib.flake-root}
    set -e
    cd ''${FLAKE_ROOT}
    ${vagrant}/bin/vagrant reload "$@"
  '');
}
