{ lib
, pkgs
, deploy ? pkgs.deploy-rs
}:

{ task-name
, config
}:

(lib.make-task task-name ''
  ${lib.flake-root}
  ${deploy}/bin/deploy ''${FLAKE_ROOT}#${config} "$@"
'')
