{ lib
, pkgs
, age ? pkgs.age
}:

{ task-name
, ip
, key
, user
, destination ? "/var/lib/sops-private-key"
, config ? ".sops.yaml"
}:

# Warning: Requires that node already has age dependency.

let
  last-key = "$(find ${destination} -type f | sort | tail -1)";
in (lib.make-task task-name ''
  ${lib.flake-root}
  set -e

  # Create age keys on node.
  ssh ${user}@${ip} '
    sudo mkdir -p ${destination}
    sudo ${age}/bin/age-keygen -o ${destination}/$(date -u +%F-%H-%M-%S)'

  # Retrieve public key from node.
  NEW_PUBKEY=$(ssh ${user}@${ip} '
    sudo ${age}/bin/age-keygen -y "${last-key}"')

  # Update public key in sops config file.
  sed \
    -i ''${FLAKE_ROOT}/${config} \
    -e "s/&${key}.*$/\&${key} ''${NEW_PUBKEY}/"

  ${lib.update-secrets { inherit config; }}/bin/nofreedisk-update-secrets
'')
