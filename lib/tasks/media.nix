{ lib
, pkgs
, rsync ? pkgs.rsync
}:

{ name
, ip
, user
, destination ? "/srv/media"
, media-dir-var ? "NOFREEDISK_MEDIA_DIR"
, args ? []
, args-var ? "NOFREEDISK_MEDIA_ARGS"
}:

let
  args-str = toString (args ++ [ "\${${args-var}}" ]);

  local = ''''${${media-dir-var}}'';
  remote = "${user}@${ip}:${destination}";

  assert-media-dir-var =
  let
    dirs = map (x: local + "/" + x) ([""] ++ [
      "musics"
      "films"
    ]);
  in ''
    for dir in ${toString dirs}
    do
      echo "''${dir}"
      if [ ! -d "''${dir}" ]
      then
        echo "ERROR : ''${dir} doesn't exist"
        exit 1
      fi
    done
  '';

  # It is important to add "/" after source, not to duplicate the root directory.
  command-dry = src: dest: "${rsync}/bin/rsync -ncrt --info=del,name,stats2 ${args-str} ${src}/ ${dest}";
  command-dryrapid = src: dest: "${rsync}/bin/rsync -nrt --info=del,name,stats2 ${args-str} ${src}/ ${dest}";
  command-sync = src: dest: "${rsync}/bin/rsync -crt --info=del,name,stats2 ${args-str} ${src}/ ${dest}";

in {
  "media-validate-dir-${name}" = (lib.make-task "media-validate-dir-${name}" ''
    ${assert-media-dir-var}
  '');

  "media-upload-diff-${name}" = (lib.make-task "media-upload-diff-${name}" '''');
  "media-upload-dry-${name}" = (lib.make-task "media-upload-dry-${name}" ''
    ${command-dry local remote}
  '');
  "media-upload-dryrapid-${name}" = (lib.make-task "media-upload-dryrapid-${name}" ''
    ${command-dryrapid local remote}
  '');
  "media-upload-sync-${name}" = (lib.make-task "media-upload-sync-${name}" ''
    ${command-sync local remote}
    set -x
    ssh ${user}@${ip} '
      find ${destination} -type d -exec chmod 755 {} +
      find ${destination} -exec chmod o+r {} +
    '
  '');

  "media-download-diff-${name}" = (lib.make-task "media-download-diff-${name}" '''');
  "media-download-dry-${name}" = (lib.make-task "media-download-dry-${name}" ''
    ${command-dry remote local}
  '');
  "media-download-dryrapid-${name}" = (lib.make-task "media-download-dryrapid-${name}" ''
    ${command-dryrapid remote local}
  '');
  "media-download-sync-${name}" = (lib.make-task "media-download-sync-${name}" ''
    ${command-sync remote local}
  '');
}
