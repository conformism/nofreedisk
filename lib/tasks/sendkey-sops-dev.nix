{ lib
, pkgs
, sops ? pkgs.sops
}:

{ task-name
, ip
, file
, destination ? "/var/lib/sops-private-key"
}:

with lib.asserts;
assert assertMsg (builtins.isString file)
  ("The file \"${toString file}\" containing the sops private key should be "
  + "given as a string, relative to project's root directory.");

(lib.make-task task-name ''
  ${lib.flake-root}
  ${lib.sops-age-key-file}
  ${lib.tmpdir}
  set -e
  ssh root@${ip} '
    mkdir -p ${destination}'
  CLEAR_KEY=''${TMPDIR}/${builtins.baseNameOf file}
  ${sops}/bin/sops -d \
    --output ''${CLEAR_KEY} \
    ''${FLAKE_ROOT}/${file}
  scp \
    ''${CLEAR_KEY} \
    root@${ip}:${destination}/$(date -u +%F-%H-%M-%S)
'')
