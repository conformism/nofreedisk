{ lib
, pkgs
, sops ? pkgs.sops
}:

{ config ? ".sops.yaml"
}:

lib.make-task "update-secrets" ''
  ${lib.flake-root}
  ${lib.sops-age-key-file}
  for secret in $(find ''${FLAKE_ROOT}/secrets -type f)
  do
    ${sops}/bin/sops \
      --config ''${FLAKE_ROOT}/${config} \
      updatekeys -y \
      ''${secret}
  done
''
