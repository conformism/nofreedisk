{ lib }:

{ task-name
, ip
, user ? "root"
}:

(lib.make-task task-name ''
  ssh ${user}@${ip} "$@"
'')
