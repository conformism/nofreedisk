{ lib
, pkgs
, sops ? pkgs.sops
, consul ? pkgs.consul
}:

{ task-name
, ca
, key
, dest ? "secrets"
, certs ? []
, config ? ".sops.yaml"
}:

with lib.asserts;
assert assertMsg (builtins.isString dest)
  ("The destination directory \"${toString dest}\" for the TLS certificates "
  + "should be given as a string, relative to project's root directory.");
assert assertMsg (builtins.isString ca)
  ("The file \"${toString ca}\" containing TLS CA certificate "
  + "should be given as a string, relative to project's root directory.");
assert assertMsg (builtins.isString key)
  ("The file \"${toString key}\" containing TLS CA private key "
  + "should be given as a string, relative to project's root directory.");
let
  allowed-kinds = [ "server" "client" "cli" ];
in assert lib.lists.all (cert:
  let
    has-kind-field = lib.attrsets.hasAttrByPath [ "kind" ] cert;
  in assertMsg
    (has-kind-field && (builtins.isString cert.kind) && (builtins.elem cert.kind allowed-kinds))
    (if has-kind-field
    then "The certs.[].kind field value \"${cert.kind}\" must be one of [${toString allowed-kinds}]."
    else "The certs.[].kind field must be present and must be one of [${toString allowed-kinds}].")
) certs;

let
  commands = lib.concatLines (map (cert:
    "${consul}/bin/consul tls cert create -${toString cert.kind} -ca=${builtins.baseNameOf ca} -key=${builtins.baseNameOf key} ${toString cert.args}"
  ) certs);

in (lib.make-task task-name ''
  ${lib.flake-root}
  ${lib.sops-age-key-file}
  ${lib.tmpdir}
  set -e

  cd ''${TMPDIR}

  for file in ${ca} ${key}
  do
    ${sops}/bin/sops \
      --config ''${FLAKE_ROOT}/${config} \
      --output $(basename ''${file}) \
      -d ''${FLAKE_ROOT}/''${file}
  done

  ${commands}

  mkdir -p ''${FLAKE_ROOT}/${dest}

  for file in $(find . -type f ! -name ${builtins.baseNameOf ca} ! -name ${builtins.baseNameOf key})
  do
    # Avoid clear secrets being written on disk :
    # symlink to destination, so that the symlink matches a sops rule,
    # encrypt in place, then move back to destination, on disk.
    ln -sf $(realpath ''${file}) ''${FLAKE_ROOT}/${dest}
    ${sops}/bin/sops \
      --config ''${FLAKE_ROOT}/${config} \
      -e -i ''${FLAKE_ROOT}/${dest}/''${file}
    mv -f ''${file} ''${FLAKE_ROOT}/${dest}
  done
'')
