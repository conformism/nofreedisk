{ lib
, pkgs
, sops ? pkgs.sops
, consul ? pkgs.consul
}:

{ task-name
, args ? ""
, dest ? "secrets"
, config ? ".sops.yaml"
}:

with lib.asserts;
assert assertMsg (builtins.isString dest)
  ("The destination directory \"${toString dest}\" for the TLS CA "
  + "should be given as a string, relative to project's root directory.");

(lib.make-task task-name ''
  ${lib.flake-root}
  ${lib.sops-age-key-file}
  ${lib.tmpdir}
  set -e

  cd ''${TMPDIR}

  ${consul}/bin/consul tls ca create ${toString args}

  mkdir -p ''${FLAKE_ROOT}/${dest}

  for file in $(find . -type f)
  do
    # Avoid clear secrets being written on disk :
    # symlink to destination, so that the symlink matches a sops rule,
    # encrypt in place, then move back to destination, on disk.
    ln -sf $(realpath ''${file}) ''${FLAKE_ROOT}/${dest}
    ${sops}/bin/sops \
      --config ''${FLAKE_ROOT}/${config} \
      -e -i ''${FLAKE_ROOT}/${dest}/''${file}
    mv -f ''${file} ''${FLAKE_ROOT}/${dest}
  done
'')
