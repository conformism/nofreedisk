{ lib
, pkgs
, sops ? pkgs.sops
, uuidgen ? pkgs.uuidgen
}:

{ task-name
, key ? ''["acl"]["tokens"]["initial_management"]''
, file
, config ? ".sops.yaml"
}:

with lib.asserts;
assert assertMsg (builtins.isString file)
  ("The file \"${toString file}\" containing Consul ACL initial management token "
  + "key should be given as a string, relative to project's root directory.");

# https://developer.hashicorp.com/consul/docs/agent/config/config-files#acl_tokens_initial_management

(lib.make-task task-name ''
  ${lib.flake-root}
  ${lib.sops-age-key-file}
  set -e

  if [ ! -f ''${FLAKE_ROOT}/${file} ]
  then
    echo "ERROR : ''${FLAKE_ROOT}/${file} doesn't exist, please create it before"
  else
    KEY='${key}'
    ${sops}/bin/sops \
      --config ''${FLAKE_ROOT}/${config} \
      --set "''${KEY} \"$(uuidgen)\"" \
      ''${FLAKE_ROOT}/${file}
  fi
'')
