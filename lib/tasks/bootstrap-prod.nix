{ lib
, nixos-anywhere
}:

{ task-name
, ip
, config
, user ? "root"
}:

let
  y = "\\033[01;33m";
  d = "\\033[00m";
in (lib.make-task task-name ''
  ${lib.flake-root}
  ssh-keygen -f "''${HOME}/.ssh/known_hosts" -R ${ip}
  echo -e "\n${y} YOU WILL TOTALLY ERASE THE TARGETED SYSTEM (${ip})," \
    "ARE YOU SURE ?${d}\n"
  ssh-copy-id \
    -o StrictHostKeyChecking=no \
    -o PreferredAuthentications=password \
    -o PasswordAuthentication=yes \
    -o ChallengeResponseAuthentication=no \
    -o GSSAPIAuthentication=no \
    -o HostbasedAuthentication=no \
    -o KbdInteractiveAuthentication=no \
    -o PubkeyAuthentication=no \
    ${user}@${ip}
  ssh ${user}@${ip} 'sudo cp -r ~/.ssh /root/'
  ${nixos-anywhere}/bin/nixos-anywhere \
    --flake ''${FLAKE_ROOT}#${config} \
    root@${ip}
  ssh-keygen -f "''${HOME}/.ssh/known_hosts" -R ${ip}
  echo -e "\nNow the server may be long to restart, please be patient...\n"
  ssh -o StrictHostKeyChecking=no -o ConnectionAttempts=20 root@${ip} true
'')
