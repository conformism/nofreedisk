{ writeTextDir
}:

color:

writeTextDir "index.html" ''
  <!DOCTYPE html>
  <html>
  <head>
  <title>Welcome to ${toString color} App!</title>
  <style>
      body {
          width: 35em;
          margin: 0 auto;
          font-family: Tahoma, Verdana, Arial, sans-serif;
          background-color: ${toString color};
      }
  </style>
  </head>
  <body>
  <h1 style=\"text-align: center;\">Welcome to ${toString color} App!</h1>
  </body>
  </html>
''
