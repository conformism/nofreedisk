{ consul
, applyPatches
, fetchpatch
, fetchFromGitHub
, buildGoModule
, yarn2nix-moretea
, yarn
, nodejs-slim
}:

# Due to several Yarn modules, building this derivation might require to increase
# the maximum number of open file descriptors on the system:
# either run `ulimit -n 2048` or add "* - nofile 2048" to `/etc/security/limits.conf`.

let
  version = "1.17.2";

  src = applyPatches {
    src = fetchFromGitHub {
      owner = "hashicorp";
      repo = "consul";
      rev = "v${version}";
      hash = "sha256-zfJZUDR6GJlJbnLIXa5duq8VOG0WUN+FP5BLQ4EgPKM=";
    };
    patches = [
      ./add-nix-branded-external-source.patch
      (fetchpatch {
        # Merge args from cmd and file while service register
        # https://github.com/hashicorp/consul/issues/19572
        url = "https://github.com/hashicorp/consul/pull/19609.patch";
        hash = "sha256-25Q+kTV9nSdgBhXL3pF1d4JEBhaRaXZMTAyj43obzLg=";
      })
    ];
  };

in (consul.overrideAttrs (old: {

  inherit src version;

  prePatch =
  let
    ui = yarn2nix-moretea.mkYarnWorkspace {
      src = "${src}/ui";
      nodejs = nodejs-slim;
      yarn = yarn.override { nodejs = nodejs-slim; };
      packageOverrides.consul-ui = {
#          # To re-enable in case nixpkgs node versions does not match anymore the one used by Hashicorp.
#          substituteInPlace packages/consul-ui/package.json --replace '"node": ">=14 <=16"' '"node": ">=14 <=18"'
#          substituteInPlace package.json --replace '"node": ">=14 <=16"' '"node": ">=14 <=18"'
        prePatch = ''
          substituteInPlace config/utils.js --replace 'git rev-parse --short HEAD' 'echo ${src.src.rev}'
          substituteInPlace config/utils.js --replace 'git show -s --format=%ci HEAD' 'echo 0000'
          substituteInPlace config/utils.js --replace '`''${repositoryRoot}' '`${src}'
        '';
        # TODO Override configurePhase to avoid node_modules useless copy.
        buildPhase = ''
          export HOME=$(mktemp -d)
          yarn run --offline build
        '';
        installPhase = ''
          mkdir -p $out
          mv deps/consul-ui/dist $out/
        '';
        doDist = false;
      };
    };
  in ''
    ls agent/uiserver/dist
    rm -rf agent/uiserver/dist
    cp -rv ${ui.consul-ui}/dist agent/uiserver/
  '';

})).override {
  buildGoModule = args: buildGoModule (args // {
    inherit src version;
    vendorHash = "sha256-bKiKfLq/kbZYziUyD2v1o9X2Gs6uiqzPSYWE4cpeU9Q=";
  });
}
