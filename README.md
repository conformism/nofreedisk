# Nofreedisk

Self hosted infrastructure based on NixOS.

## Dependencies

- Nix
- Libvirt enabled
- Nix config : `sandbox = relaxed`
- Sops Age key :

| Nix install type | Key file | Owners | Rights
|-|-|-|-|
| Multi-user | `/etc/sops/age/nofreedisk.txt` | `root:nixbld` | `640` |
| Single-user | `~/.config/sops/age/nofreedisk.txt` | `$(whoami):$(whoami)` | `600` |

## Production prerequisites

- It seems on Kimsufi servers that Ubuntu images behave better than Debian ones to be replaced by NixOS.

## Usage

### Local dev cluster

```sh
nix flake update
nix develop

    nofreedisk-help

    # Create new secrets & new admin key if needed.
    rm -rf secrets # Also edit .sops.yaml if needed.
    nofreedisk-rotate-age-admin-key --force <admin_name>
    nofreedisk-rotate-age-admin-key --gc
    nofreedisk-create-vanilla-secrets
    sops secrets/<file> # Edit secrets.

    # Deploy 2 Debian VMs. (dev only)
    nofreedisk-vagrant-up-dev
    nofreedisk-vagrant-reload-dev # May be required after a host reboot: sometimes Libvirt
                                  # takes our settings in account after specified random ones first.

    # Repartition VM disks & install NixOS in place of Debian.
    nofreedisk-bootstrap-dev-node1
    nofreedisk-bootstrap-dev-node2

    # Send master private key. (dev only)
    nofreedisk-sendkey-sops-dev-node1
    nofreedisk-sendkey-sops-dev-node2

    # Or (re)generate master private key if needed.
    nofreedisk-keygen-sops-dev-node1
    nofreedisk-keygen-sops-dev-node2

    # Regenerate admin key if needed.
    nofreedisk-rotate-age-admin-key <admin_name>
    # Commit before removing old key.
    nofreedisk-rotate-age-admin-key --gc

    # Regenerate Consul gossip encryption key if needed.
    nofreedisk-rotate-consul-gossip-key-dev

    # Regenerate Consul initial management ACL token if needed.
    nofreedisk-rotate-consul-acl-initial-management-dev

    # Regenerate Consul TLS CA and certs if needed.
    nofreedisk-rotate-consul-tls-ca-dev
    nofreedisk-rotate-consul-tls-certs-dev

    # Update NixOS.
    nofreedisk-deploy-dev-node1
    nofreedisk-deploy-dev-node2

    # Enter a VM.
    nofreedisk-ssh-dev-node1
    vagrant ssh node1
→       vagrant

    # Deploy applicative layer into Hashistack cluster.
    nofreedisk-terraform-apply-dev
```

## Cool links

### Similar projects

- https://github.com/astro/skyflake
- https://github.com/input-output-hk/bitte
- https://github.com/datakurre/nomad-with-nix
- https://github.com/viperML/neoinfra
- https://git.sr.ht/~magic_rb/cluster
- https://github.com/nairnavin/practical-nomad-consul
- https://gitlab.com/coffeetables/myrdd
- https://github.com/TUM-DSE/doctor-cluster-config
- https://git.deuxfleurs.fr/Deuxfleurs/nixcfg
- https://github.com/serokell/gemini-infra
- https://github.com/wkennington/nixos
- https://git.freumh.org/ryan/nixos

### Useful tools

- https://git.deuxfleurs.fr/Deuxfleurs/nomad-driver-nix2
- https://github.com/MagicRB/nomad-driver-containerd-nix
- https://github.com/astro/microvm.nix
- https://github.com/tweag/tf-ncl
- https://github.com/terranix/terranix
- https://github.com/tristanpemble/nix-nomad

### Resources

- [NixOS Search](https://search.nixos.org)
- [Nomad deployment guide](https://developer.hashicorp.com/nomad/tutorials/enterprise/production-deployment-guide-vm-with-consul)
- [Nomad reference architecture](https://developer.hashicorp.com/nomad/tutorials/enterprise/production-reference-architecture-vm-with-consul)
- [Consul deployment guide](https://developer.hashicorp.com/consul/tutorials/production-deploy/deployment-guide)
- [Consul reference architecture](https://developer.hashicorp.com/consul/tutorials/production-deploy/reference-architecture)
