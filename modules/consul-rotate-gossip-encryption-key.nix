{ config
, options
, lib
, pkgs
, ...
}:

with lib;

let
  cfg = config.services.consul-rotate-gossip-encryption-key;

  consul-rotate-gossip-encryption-key = pkgs.writers.writeBashBin "consul-rotate-gossip-encryption-key" ''
    # https://developer.hashicorp.com/consul/tutorials/datacenter-operations/gossip-encryption-rotate#create-a-script-for-rotating-the-gossip-key

    set -e

    echo "CONSUL_HTTP_ADDR=$CONSUL_HTTP_ADDR"

    # Get the new key
    NEW_KEY=$(${pkgs.jq}/bin/jq -r '.encrypt' "/run/secrets/consul-encrypt.json")

    # Install the key
    ${pkgs.consul}/bin/consul keyring -install ''${NEW_KEY}

    # Set as primary
    ${pkgs.consul}/bin/consul keyring -use ''${NEW_KEY}

    # Retrieve all keys used by Consul
    KEYS=$(${pkgs.curl}/bin/curl -s ''${CONSUL_HTTP_ADDR}/v1/operator/keyring)
    ALL_KEYS=$(echo ''${KEYS} | ${pkgs.jq}/bin/jq -r '.[].Keys| to_entries[].key' | sort | uniq)

    # Remove other keys
    for i in $(echo ''${ALL_KEYS})
    do
      if [ ''${i} != ''${NEW_KEY} ]
      then
        ${pkgs.consul}/bin/consul keyring -remove ''${i}
      fi
    done
  '';

in {
  options.services.consul-rotate-gossip-encryption-key = {
    enable = mkOption {
      type = types.bool;
      default = false;
      description = mdDoc ''
        Enables the consul-rotate-gossip-encryption-key script.
        It must be triggered by a modification of the file containing the key.
        Note this doesn't have to be enabled on more than one server per cluster
        and has no effect on clients.
      '';
    };

#    # https://github.com/NixOS/nixpkgs/issues/49415
#    keyFile = mkOption {
#      type = types.nullOr types.str;
#      default = null;
#      description = mdDoc ''
#        Key file of which a modification will trigger the script. Note this
#        behavior can also be achieved with sops-nix 'restartUnits' field.
#      '';
#    };
  };

  config = mkIf (cfg.enable && config.services.consul.enable) {
    systemd.services.consul-rotate-gossip-encryption-key = mkIf config.services.consul.extraConfig.server {
      # We dont want it to run because of a reboot.
      # wantedBy = [ "multi-user.target" ];
      wants = [ "consul.service" ];
      after = [ "consul.service" ];
#      restartTriggers = mkIf (cfg.keyFile != null) [ cfg.keyFile ];
      serviceConfig = {
        Type = "oneshot";
        ExecStart = "${consul-rotate-gossip-encryption-key}/bin/consul-rotate-gossip-encryption-key";
        Environment = "CONSUL_HTTP_ADDR=http://localhost:8500";
      };
    };
  };
}
