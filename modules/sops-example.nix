{ config
, pkgs
, ...
}:

{
  config = {
    environment.systemPackages = [
      (pkgs.writeShellScriptBin "sops-example" ''
        ${pkgs.bat}/bin/bat ${config.sops.secrets."example".path}
      '')
    ];
  };
}
