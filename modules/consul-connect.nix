{ config
, options
, lib
, pkgs
, ...
}:

with lib;

# https://developer.hashicorp.com/consul/docs/connect/proxies/deploy-service-mesh-proxies
# https://www.oreilly.com/library/view/consul-up-and/9781098106133/ch04.html
# https://discuss.hashicorp.com/t/how-do-sidecar-proxies-work-without-containers/33553/6

let
  cfg = config.services.consul-connect;

in {
  options.services.consul-connect = {
    enable = mkOption {
      type = types.bool;
      default = false;
      description = mdDoc ''
        Register some Consul services and deploy some Envoy sidecar proxies and
        gateways. Requires an operational Consul cluster with service mesh
        (Consul Connect) enabled.
      '';
    };
    enableServices = mkOption {
      type = types.bool;
      default = true;
      description = mdDoc ''
        Enable all the services.
      '';
    };
    enableSidecarProxies = mkOption {
      type = types.bool;
      default = true;
      description = mdDoc ''
        Enable all the sidecar proxies.
      '';
    };
    enableGateways = mkOption {
      type = types.bool;
      default = true;
      description = mdDoc ''
        Enable all the gateways.
      '';
    };
    enableInlineCertificates = mkOption {
      type = types.bool;
      default = true;
      description = mdDoc ''
        Enable all the inline certificate config entries.
      '';
    };
    enableConfigEntries = mkOption {
      type = types.bool;
      default = true;
      description = mdDoc ''
        Enable all the config entries.
      '';
    };
    avoidSystemdDependencies = mkOption {
      type = types.bool;
      default = false;
      description = mdDoc ''
        Disable Systemd dependencies. Useful in case of services running on
        different machines (in containers for instance). The Consul API is used
        anyway by services to wait for Consul to be up and by sidecar proxies
        and gateways to wait for counterpart services to be registered.
        Prevent sidecar-proxies.[].attachTo and gateways.[].attachTo to attach
        implicitely to their services counterparts.
      '';
    };
    avoidSystemdDependenciesToConsul = mkOption {
      type = types.bool;
      default = false;
      description = mdDoc ''
        Disable Systemd dependencies to Consul. Useful in case of services running on
        different machines (in containers for instance). The Consul API is used
        anyway by services to wait for Consul to be up.
      '';
    };

    services = mkOption {
      default = {};
      type = types.attrsOf (types.submodule ({ config, ... }: {
        options = {
          enable = mkOption {
            type = types.bool;
            default = true;
            description = mdDoc ''
              Enable the service.
            '';
          };
#          file = mkOption {
#            type = types.nullOr types.path;
#            default = null;
#            apply = value:
#              assert lib.asserts.assertMsg (config._module.args.config != null && value != null)
#                "Config and file cannot be set at the same time";
#              value;
#            description = mdDoc ''
#              Service configuration file, can be both JSON or HCL.
#            '';
#          };
          config = mkOption {
            type = types.attrsOf types.anything;
            default = {};
            description = mdDoc ''
              Service configuration that will be serialized to JSON.
              Cf. https://developer.hashicorp.com/consul/docs/services/configuration/services-configuration-reference
            '';
          };
          args = mkOption {
            type = types.listOf types.singleLineStr;
            default = [];
            description = mdDoc ''
              Additionnal command line arguments that will be passed to Consul
              for both registering and deregistering the service.
            '';
          };
        };
      }));
    };

    sidecar-proxies = mkOption {
      default = {};
      type = types.attrsOf (types.submodule ({ config, ... }: {
        options = {
          enable = mkOption {
            type = types.bool;
            default = true;
            description = mdDoc ''
              Enable the sidecar proxy.
            '';
          };
          attachTo = mkOption {
            type = types.nullOr types.singleLineStr;
            default = if cfg.avoidSystemdDependencies
              then null
              else config._module.args.name;
            description = mdDoc ''
              Automatically retrieve `service` and `id` from the specified service.
              Will automatically attach to the name-matching service unless
              services.consul-connect.avoidSystemdDependencies is set. Has no
              effect if `service` and `id` are set manually.
            '';
          };
          service = mkOption {
            type = types.nullOr types.singleLineStr;
            default =
            let
              attachTo = cfg.sidecar-proxies.${config._module.args.name}.attachTo;
            in if (attachTo != null && (lib.attrsets.hasAttrByPath [ attachTo "config" "service" "name" ] cfg.services))
              then cfg.services.${attachTo}.config.service.name
              else null;
            apply = value:
              assert lib.asserts.assertMsg (value != null)
                ("The field services.consul-connect.sidecar-proxies.${config._module.args.name}.service "
                + "must not be null. Set it to an existant Consul service name. You can either set it manually, "
                + "or use services.consul-connect.sidecar-proxies.${config._module.args.name}.attachTo to "
                + "refer to an existant services.consul-connect.services.<attachTo>.config.name field.");
              value;
            description = mdDoc ''
              Name of the sidecar proxy, should match a Consul service.
            '';
          };
          id = mkOption {
            type = types.nullOr types.singleLineStr;
            default =
            let
              attachTo = cfg.sidecar-proxies.${config._module.args.name}.attachTo;
            in if (attachTo != null && (lib.attrsets.hasAttrByPath [ attachTo "config" "service" "id" ] cfg.services))
              then cfg.services.${attachTo}.config.service.id
              else null;
            apply = value:
              assert lib.asserts.assertMsg (value != null)
                ("The field services.consul-connect.sidecar-proxies.${config._module.args.name}.id "
                + "must not be null. Set it to an existant Consul service ID. You can either set it manually, "
                + "or use services.consul-connect.sidecar-proxies.${config._module.args.name}.attachTo to "
                + "refer to an existant services.consul-connect.services.<attachTo>.config.id field.");
              value;
            description = mdDoc ''
              ID of the sidecar proxy, should match a Consul service ID.
            '';
          };
          adminBind = mkOption {
            type = types.singleLineStr;
            default = "localhost:19000";
            apply = value: "-admin-bind=${value}";
            description = mdDoc ''
              The host:port to bind Envoy's admin HTTP API. Envoy requires that
              this be enabled. The host part must be resolvable DNS name or IP
              address. Be careful to choose non conflicting value between your
              various proxies.
            '';
          };
          args = mkOption {
            type = types.listOf types.singleLineStr;
            default = [];
            description = mdDoc ''
              Additionnal command line arguments.
            '';
          };
        };
      }));
    };

    gateways = mkOption {
      default = {};
      type = types.attrsOf (types.submodule ({ config, ... }: {
        options = {
          enable = mkOption {
            type = types.bool;
            default = true;
            description = mdDoc ''
              Enable the gateway.
            '';
          };
          attachTo = mkOption {
            type = types.nullOr types.singleLineStr;
            default = if cfg.avoidSystemdDependencies
              then null
              else config._module.args.name;
            description = mdDoc ''
              Automatically retrieve `service` and `id` from the specified service.
              Will automatically attach to the name-matching service unless
              services.consul-connect.avoidSystemdDependencies is set. Has no
              effect if `service` and `id` are set manually.
            '';
          };
          service = mkOption {
            type = types.nullOr types.singleLineStr;
            default =
            let
              attachTo = cfg.gateways.${config._module.args.name}.attachTo;
            in if (attachTo != null && (lib.attrsets.hasAttrByPath [ attachTo "config" "service" "name" ] cfg.services))
              then cfg.services.${attachTo}.config.service.name
              else null;
            apply = value:
              assert lib.asserts.assertMsg (value != null)
                ("The field services.consul-connect.gateways.${config._module.args.name}.service "
                + "must not be null. Set it to an existant Consul service name. You can either set it manually, "
                + "or use services.consul-connect.gateways.${config._module.args.name}.attachTo to "
                + "refer to an existant services.consul-connect.services.<attachTo>.config.name field.");
              value;
            description = mdDoc ''
              Name of the gateway, should match a Consul service.
            '';
          };
          id = mkOption {
            type = types.nullOr types.singleLineStr;
            default =
            let
              attachTo = cfg.gateways.${config._module.args.name}.attachTo;
            in if (attachTo != null && (lib.attrsets.hasAttrByPath [ attachTo "config" "service" "id" ] cfg.services))
              then cfg.services.${attachTo}.config.service.id
              else null;
            apply = value:
              assert lib.asserts.assertMsg (value != null)
                ("The field services.consul-connect.gateways.${config._module.args.name}.id "
                + "must not be null. Set it to an existant Consul service ID. You can either set it manually, "
                + "or use services.consul-connect.gateways.${config._module.args.name}.attachTo to "
                + "refer to an existant services.consul-connect.services.<attachTo>.config.id field.");
              value;
            description = mdDoc ''
              ID of the gateway, should match a Consul service ID.
            '';
          };
          adminBind = mkOption {
            type = types.singleLineStr;
            default = "localhost:19000";
            apply = value: "-admin-bind=${value}";
            description = mdDoc ''
              The host:port to bind Envoy's admin HTTP API. Envoy requires that
              this be enabled. The host part must be resolvable DNS name or IP
              address. Be careful to choose non conflicting value between your
              various proxies.
            '';
          };
          args = mkOption {
            type = types.listOf types.singleLineStr;
            default = [];
            description = mdDoc ''
              Additionnal command line arguments.
            '';
          };
        };
      }));
    };

    inline-certificates = mkOption {
      default = {};
      type = types.attrsOf (types.submodule ({ config, ... }: {
        options = {
          enable = mkOption {
            type = types.bool;
            default = true;
            description = mdDoc ''
              Enable the inline certificate.
            '';
          };
          useACMEHost = mkOption {
            type = types.nullOr types.singleLineStr;
            default = null;
            apply = value:
              assert lib.asserts.assertMsg (value != null -> (cfg.inline-certificates.${config._module.args.name}.cert == null && cfg.inline-certificates.${config._module.args.name}.key == null)) ''
                useACMEHost and cert & key cannot be set at the same time.
                  useACMEHost = ${toString value}
                  cert = ${toString cfg.inline-certificates.${config._module.args.name}.cert}
                  key = ${toString cfg.inline-certificates.${config._module.args.name}.key}
              '';
              value;
            description = mdDoc ''
              Use a Let's Encrypt certificate created with `security.acme.certs`.
            '';
          };
          cert = mkOption {
            type = types.nullOr types.singleLineStr;
            default = null;
            description = mdDoc ''
              Provide arbitrary certificate as file.
            '';
          };
          key = mkOption {
            type = types.nullOr types.singleLineStr;
            default = null;
            description = mdDoc ''
              Provide arbitrary certificate private key as file.
            '';
          };
        };
      }));
    };

    config-entries = mkOption {
      default = {};
      type = types.attrsOf (types.submodule ({ config, ... }: {
        options = {
          enable = mkOption {
            type = types.bool;
            default = true;
            description = mdDoc ''
              Enable the config entry.
            '';
          };
          force = mkOption {
            type = types.bool;
            default = false;
            description = mdDoc ''
              Delete and rewrite config entry at each deployment. This can help
              to rectify modifications made by another way.
            '';
          };
          config = mkOption {
            type = types.attrsOf types.anything;
            default = {};
            description = mdDoc ''
              Configuration entry that will be serialized to JSON.
              Cf. https://developer.hashicorp.com/consul/docs/connect/config-entries
            '';
          };
        };
      }));
    };
  };

  config.systemd.targets.consul-connect = mkIf (cfg.enable && config.services.consul.enable) {
    wantedBy = [ "multi-user.target" ];
  };

  config.systemd.services =
  # Warning: This won't protect if some gateways / sidecar proxies are containerized
  # and thus in different NixOS configs but still share the host' network ports.
  assert lib.lists.all (x: x == true)
    (map
      (group: assertMsg (builtins.length group == 1) ''
          Conflicting Consul Connect adminBind values on gateways / sidecar proxies:
          ${concatLines (lib.lists.forEach group (v: "  name: ${v.name}\t\tadminBind: ${removePrefix "-admin-bind=" v.adminBind}"))}
      '')
      (mapAttrsToList (n: v: v)
        (builtins.groupBy (x: x.adminBind) (
          let
            format = list: mapAttrsToList (name: value: { inherit name; inherit (value) adminBind; }) list;
          in format cfg.sidecar-proxies
          ++ format cfg.gateways
        ))
      )
    );

  let
    # TODO set this through options
    export-env = ''
      export CONSUL_CLIENT_CERT=${config.environment.variables.CONSUL_CLIENT_CERT}
      export CONSUL_CLIENT_KEY=${config.environment.variables.CONSUL_CLIENT_KEY}
      export CONSUL_CACERT=${config.environment.variables.CONSUL_CACERT}
      export CONSUL_HTTP_TOKEN=${config.environment.variables.CONSUL_HTTP_TOKEN}
      export CONSUL_HTTP_ADDR=${config.environment.variables.CONSUL_HTTP_ADDR}
    '';

    # This waits for Consul and some Consul services to be available without
    # using systemd mechanics. This is useful both because:
    # - Systemd Consul service becomes 'active' before Consul is actually able
    #   to recieve some API directives.
    # - It allows to establish dependency links across different machines, which
    #   is useful in case of NixOS managed VMs/containers containing some
    #   services and/or Envoy proxies.
    wait-for-consul = pkgs.writers.writeBash "consul-wait-for-service-consul" (export-env + ''
      until ${pkgs.consul}/bin/consul catalog services | grep -x consul
      do
        sleep 1
      done
    '');

    wait-for-service = service: id: pkgs.writers.writeBash "consul-wait-for-service-${id}" (export-env + ''
      exec 1>&2

      until ${pkgs.curl}/bin/curl \
        --cert "''${CONSUL_CLIENT_CERT}" \
        --key "''${CONSUL_CLIENT_KEY}" \
        --cacert "''${CONSUL_CACERT}" \
        --header "X-Consul-Token: ''${CONSUL_HTTP_TOKEN}" \
        ''${CONSUL_HTTP_ADDR}/v1/catalog/service/${service} \
      | ${pkgs.jq}/bin/jq \
        --exit-status \
        '.[].ServiceID | select(. == "${id}")'
      do
        sleep 0.1
      done
      # Strange enough, this padding seems to work more reliably than wrapping
      # service registration in a Consul lock and waiting here for it to be released.
      sleep 1
    '');

  in mkIf (cfg.enable && (cfg.avoidSystemdDependencies || cfg.avoidSystemdDependenciesToConsul || config.services.consul.enable)) (mkMerge [
    (mkIf cfg.enableServices (attrsets.mapAttrs' (name: service:
      let
        meta = ''-meta=external-source=nix'';
        file = builtins.toFile "${name}.json" (builtins.toJSON service.config);
      in nameValuePair
      "consul-${name}-service" (mkIf service.enable {
        description = "Consul service mesh service ${name}";
        wantedBy = [ "consul-connect.target" ];
        wants = mkIf (cfg.avoidSystemdDependencies == false && cfg.avoidSystemdDependenciesToConsul == false) [ "consul.service" ];
        after = mkMerge [
          [ "network.target" ]
          (mkIf (cfg.avoidSystemdDependencies == false && cfg.avoidSystemdDependenciesToConsul == false) [ "consul.service" ])
        ];
        partOf = [ "consul-connect.target" ];
        preStop = "";
        serviceConfig = {
          Type = "oneshot";
          ExecStartPre = wait-for-consul;
          ExecStart = pkgs.writers.writeBash "consul-${name}-service-start" (export-env + ''
            ${pkgs.consul}/bin/consul services register ${meta} ${toString service.args} ${file}
          '');
          ExecStop = pkgs.writers.writeBash "consul-${name}-service-stop" (export-env + ''
            ${pkgs.consul}/bin/consul services deregister ${toString service.args} ${file}
          '');
          RemainAfterExit = true;
          Restart = "on-failure";
          RestartSec = 5;
        };
      }
    )) cfg.services))

    (mkIf cfg.enableSidecarProxies (attrsets.mapAttrs' (name: proxy:
      nameValuePair
      "consul-${name}-sidecar-proxy" (mkIf proxy.enable {
        description = "Consul service mesh Envoy proxy for service ${name}";
        wantedBy = [ "consul-connect.target" ];
        wants = mkMerge [
          (mkIf (cfg.avoidSystemdDependencies == false && cfg.avoidSystemdDependenciesToConsul == false) [ "consul.service" ])
          (mkIf (cfg.avoidSystemdDependencies == false) [ "consul-${proxy.attachTo}-service.service" ])
        ];
        after = mkMerge [
          [ "network.target" ]
          (mkIf (cfg.avoidSystemdDependencies == false && cfg.avoidSystemdDependenciesToConsul == false) [ "consul.service" ])
          (mkIf (cfg.avoidSystemdDependencies == false) [ "consul-${proxy.attachTo}-service.service" ])
        ];
        partOf = mkMerge [
          [ "consul-connect.target" ]
          (mkIf (cfg.avoidSystemdDependencies == false) [ "consul-${proxy.attachTo}-service.service" ])
        ];
        preStop = "";
        serviceConfig = {
          Type = "exec";
          ExecStartPre = wait-for-service proxy.service proxy.id;
          ExecStart = pkgs.writers.writeBash "consul-${name}-sidecar-proxy-start" (export-env + ''
            ${pkgs.consul}/bin/consul \
              connect envoy \
              -envoy-binary=${pkgs.envoy}/bin/envoy \
              -sidecar-for=${toString proxy.id} \
              ${toString proxy.adminBind} \
              ${toString proxy.args}
          '');
          Restart = "always";
          RestartSec = 5;
        };
      }
    )) cfg.sidecar-proxies))

    (mkIf cfg.enableGateways (attrsets.mapAttrs' (name: gateway:
      nameValuePair
      "consul-${name}-gateway" (mkIf gateway.enable {
        description = "Consul service mesh Envoy gateway ${name}";
        wantedBy = [ "consul-connect.target" ];
        wants = mkMerge [
          (mkIf (cfg.avoidSystemdDependencies == false && cfg.avoidSystemdDependenciesToConsul == false) [ "consul.service" ])
          (mkIf (cfg.avoidSystemdDependencies == false) [ "consul-${gateway.attachTo}-service.service" ])
        ];
        after = mkMerge [
          [ "network.target" ]
          (mkIf (cfg.avoidSystemdDependencies == false && cfg.avoidSystemdDependenciesToConsul == false) [ "consul.service" ])
          (mkIf (cfg.avoidSystemdDependencies == false) [ "consul-${gateway.attachTo}-service.service" ])
        ];
        partOf = mkMerge [
          [ "consul-connect.target" ]
          (mkIf (cfg.avoidSystemdDependencies == false) [ "consul-${gateway.attachTo}-service.service" ])
        ];
        serviceConfig = {
          Type = "exec";
          ExecStartPre = wait-for-service gateway.service gateway.id;
          ExecStart = pkgs.writers.writeBash "consul-${name}-gateway-start" (export-env + ''
            ${pkgs.consul}/bin/consul \
              connect envoy \
              -envoy-binary=${pkgs.envoy}/bin/envoy \
              -proxy-id=${toString gateway.id} \
              ${toString gateway.adminBind} \
              ${toString gateway.args}
          '');
          Restart = "always";
          RestartSec = 5;
        };
      }
    )) cfg.gateways))

    (mkIf cfg.enableInlineCertificates (attrsets.mapAttrs' (name: inline-certificate:
      let
        cert = if inline-certificate.useACMEHost != null
          then "${config.security.acme.certs.${inline-certificate.useACMEHost}.directory}/cert.pem"
          else inline-certificate.cert;
        key = if inline-certificate.useACMEHost != null
          then "${config.security.acme.certs.${inline-certificate.useACMEHost}.directory}/key.pem"
          else inline-certificate.key;
        start = pkgs.writers.writeBash "consul-${name}-inline-certificate-start" (export-env + ''
          ${pkgs.openssl}/bin/openssl x509 -text -noout -in ${cert}
          ${pkgs.jq}/bin/jq \
            --null-input \
            --arg cert "$(cat ${cert})" \
            --arg key "$(cat ${key})" \
            '{Kind: "inline-certificate", Name:"${name}", Certificate: $cert, PrivateKey: $key}' \
          | ${pkgs.consul}/bin/consul config write -
        '');
      in nameValuePair
      "consul-${name}-inline-certificate" (mkIf inline-certificate.enable {
        description = "Consul service mesh inline certificate ${name}";
        wantedBy = [ "consul-connect.target" ];
        wants = mkMerge [
          (mkIf (cfg.avoidSystemdDependencies == false && cfg.avoidSystemdDependenciesToConsul == false) [ "consul.service" ])
          (mkIf (inline-certificate.useACMEHost != null) [ "acme-finished-${inline-certificate.useACMEHost}.target" ])
        ];
        after = mkMerge [
          [ "network.target" ]
          (mkIf (cfg.avoidSystemdDependencies == false && cfg.avoidSystemdDependenciesToConsul == false) [ "consul.service" ])
          (mkIf (inline-certificate.useACMEHost != null) [ "acme-finished-${inline-certificate.useACMEHost}.target" ])
        ];
        requires = mkIf (inline-certificate.useACMEHost != null) [ "acme-finished-${inline-certificate.useACMEHost}.target" ];
        partOf = mkMerge [
          [ "consul-connect.target" ]
          (mkIf (inline-certificate.useACMEHost != null) [ "acme-finished-${inline-certificate.useACMEHost}.target" ])
        ];
        restartTriggers = [ cert key ];
        serviceConfig = {
          Type = "oneshot";
          ExecStartPre = wait-for-consul;
          ExecStart = start;
          ExecReload = start;
          ExecStop = pkgs.writers.writeBash "consul-${name}-inline-certificate-stop" (export-env + ''
            ${pkgs.consul}/bin/consul config delete -kind inline-certificate -name ${name}
          '');
          RemainAfterExit = true;
          Restart = "on-failure";
          RestartSec = 5;
        };
      }
    )) cfg.inline-certificates))

    (mkIf cfg.enableConfigEntries (attrsets.mapAttrs' (name: config-entry:
      let
        file = builtins.toFile "${name}.json" (builtins.toJSON config-entry.config);
      in nameValuePair
      "consul-${name}-config-entry" (mkIf config-entry.enable {
        description = "Consul service mesh config entry ${name}";
        wantedBy = [ "consul-connect.target" ];
        wants = mkIf (cfg.avoidSystemdDependencies == false && cfg.avoidSystemdDependenciesToConsul == false) [ "consul.service" ];
        after = mkMerge [
          [ "network.target" ]
          (mkIf (cfg.avoidSystemdDependencies == false && cfg.avoidSystemdDependenciesToConsul == false) [ "consul.service" ])
        ];
        partOf = [ "consul-connect.target" ];
        serviceConfig = {
          Type = "oneshot";
          ExecStartPre = wait-for-consul;
          ExecStart = pkgs.writers.writeBash "consul-${name}-config-entry-start" (export-env + ''
            ${pkgs.consul}/bin/consul config write ${file}
          '');
          ExecStop = pkgs.writers.writeBash "consul-${name}-config-entry-stop" (export-env + ''
            ${pkgs.consul}/bin/consul config delete -filename ${file}
          '');
          RemainAfterExit = true;
          Restart = "on-failure";
          RestartSec = 5;
        };
      }
    )) cfg.config-entries))
  ]);

  config.security.acme = mkIf (cfg.enable && cfg.enableInlineCertificates)
    (attrsets.mapAttrs' (name: inline-certificate:
      nameValuePair
      "certs" (mkIf (inline-certificate.useACMEHost != null) {
        "${inline-certificate.useACMEHost}".reloadServices = [
          "consul-${name}-inline-certificate.service"
        ];
      })
    ) cfg.inline-certificates);

  config.system.activationScripts = mkIf (cfg.enable && cfg.enableConfigEntries)
    (attrsets.mapAttrs' (name: config-entry:
      nameValuePair
      "consul-${name}-config-entry-force-restart" (mkIf (config-entry.enable && config-entry.force) {
        supportsDryActivation = true;
        text = "${pkgs.systemd}/bin/systemctl stop consul-${name}-config-entry";
      }
    )) cfg.config-entries);
}
