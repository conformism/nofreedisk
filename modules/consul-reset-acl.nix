{ config
, options
, lib
, pkgs
, ...
}:

# https://developer.hashicorp.com/consul/tutorials/security/access-control-troubleshoot#reset-the-acl-system

with lib;

let
  cfg = config.services.consul-reset-acl;

  # Consul data_dir is currently not settable nor accessible in its NixOS module.
  data-dir = "/var/lib/consul";

  # TODO Reset using initial management token instead of manual bootstrap.
  consul-reset-acl = pkgs.writers.writeBashBin "consul-reset-acl" ''
    REGEX="\(reset index: ([0-9]+)\)"

    # If the first bootstrap command works fine, the ACL system just get bootstrapped,
    # no need to do the reset procedure.
    STR="$(${pkgs.consul}/bin/consul acl bootstrap 2>&1)" \
      && echo "''${STR}" \
      && exit 0

    if [[ ''${STR} =~ ''${REGEX} ]]
    then
      echo ''${BASH_REMATCH[1]} > ${data-dir}/acl-bootstrap-reset
      ${pkgs.consul}/bin/consul acl bootstrap
    else
      echo "''${STR}"
      echo "ERROR : Cannot retrieve Consul ACL reset index"
      exit 1
    fi
  '';

in {
#  options.services.consul-reset-acl = {
#    enable = mkOption {
#      type = types.bool;
#      default = false;
#      description = mdDoc ''
#        Enables the consul-reset-acl script.
#        It must be triggered manually in case of an emergency.
#      '';
#    };
#  };

#  config = mkIf (cfg.enable && config.services.consul.enable && config.services.consul.extraConfig.server) {
  config = mkIf (config.services.consul.enable && config.services.consul.extraConfig.server) {
    environment.systemPackages = [
      consul-reset-acl
    ];
  };
}
