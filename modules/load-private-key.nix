{ config
, options
, lib
, pkgs
, ...
}:

with lib;

let
  cfg = config.load-private-key;

in {
  options.load-private-key = {
    from = mkOption {
      type = types.str;
      default = "/var/lib/sops-private-key";
      description = mdDoc ''
        Path from which to load the updated sops private key
      '';
    };
  };

  config = {
    environment.systemPackages = [
      pkgs.age
    ];

    sops = {
      age.generateKey = false;
      age.sshKeyPaths = [];
      gnupg.sshKeyPaths = [];
    };

    system.activationScripts =
    let
      last-key = "$(find ${cfg.from} -type f | sort | tail -1)";
    in {
      # _aaa_ is a way to force this script to run before sops-nix' setupSecrets
      # because there is currently no good way to do it as simple as for the
      # other way with deps.
      # https://discourse.nixos.org/t/reverse-dependency-in-the-activation-script/6186
      _aaa_load-private-key = {
        supportsDryActivation = false;
        text = ''
          if [ ! -d ${cfg.from} ] \
          || [ "$(find ${cfg.from} -type f | wc -l)" = 0 ]
          then
            echo "ERROR : There is no sops private key to symlink to ${config.sops.age.keyFile}, deploy one first." >&2
            exit 1
          fi

          mkdir -p ${removeSuffix (builtins.baseNameOf config.sops.age.keyFile) config.sops.age.keyFile}
          ln -sfn "${last-key}" ${config.sops.age.keyFile}
        '';
      };

      clean-old-private-keys = {
        supportsDryActivation = false;
        deps = [ "setupSecrets" ];
        text = ''
            rm -f $(find ${cfg.from} -type f -not -wholename "${last-key}")
        '';
      };
    };
  };
}
