# Secrets

Managing secrets is complicated in itself, and it is way worse if wanting to do it declaratively. That's why it deserves a whole documentation page. Note this documents is both an explanation section about _Secrets with Nix & its ecosystem_, and a _How-to deal with secrets on a Nofreedisk cluster_.

There are different secret categories that are managed differently.

Using an external secret provider such as [Vault](https://www.vaultproject.io/) can simplify the process at different stages but relying on an external provider is a no-go solution for us.

## NixOS secrets

### Context

The most famous case and thus the most suited one : many tools already exist for that, [sops-nix](https://github.com/Mic92/sops-nix), [agenix](https://github.com/ryantm/agenix) and its variant like [ragenix](https://github.com/yaxitech/ragenix), [vault-secrets](https://github.com/serokell/vault-secrets), the ones that are integrated with deployments tools such as [NixOps](https://github.com/NixOS/nixops), [krops](https://github.com/krebs/krops), etc.

Here we use the former one because of [sops](https://github.com/getsops/sops) usage that will serve as a basis to manage other secrets categories, ending up in a more coherent global solution than if we started from raw [age](https://github.com/FiloSottile/age) usage instead, for instance.

### Usage

- NixOS config :
```nix
  imports = [
    sops-nix.nixosModules.sops
    ./modules/sops-example.nix
  ];
  sops = {
    age.keyFile = "/var/lib/sops-nix/key.txt";
#   defaultSopsKey = "";
    defaultSopsFormat = "json";
    defaultSopsFile = ./secrets/nixos.json;
    secrets = {
      "nixos.json" = {
        sopsFile = ./secrets/nixos.json;
        format = "json";
        key = "";
      };
      "example" = {};
    };
  };
```

- `modules/sops-example.nix`
```nix
{ config, pkgs, ... }:

{
  config = {
    environment.systemPackages = [
      (pkgs.writeShellScriptBin "sops-example" ''
        ${pkgs.bat}/bin/bat  ${config.sops.secrets."example".path}
      '')
    ];
  };
}
```

## Nix Privates

### Context

Nix Privates are at Nix level, required at evaluation time, unlike NixOS secrets required at NixOS activation time.
Fundamentally impure since private key is a side effect on purpose, so no solution is totally satisfying because of flakes strong opinion to enforce purity.
Here we can either delegate the impure decryption task to another layer like git with [git-crypt](https://github.com/AGWA/git-crypt), [git-agecrypt](https://github.com/vlaci/git-agecrypt), or relying on [direnv associated with sops](https://discourse.nixos.org/t/declarative-flake-secrets-when-sharing-with-non-nixos-colleagues/21673/5).
Either let the impurity at Nix level and use dirty hacks to make Nix compliant with that. No tool already do that out of the box, but there are some blog posts out there that explore various ways, combining various tools, such as ["Nix packaging, the heretic way"](https://zimbatm.com/notes/nix-packaging-the-heretic-way) or ["Secure, Declarative Key Management with NixOps, Pass, and nix-plugins"](https://elvishjerricco.github.io/2018/06/24/secure-declarative-key-management.html) breaking purity thanks to derivations' advanced attributes or Nix' plugins, respectively.
Either put secrets in a private git repository, encrypted or not.

Here we choose the dirty hack way. Also putting it encrypted in another repository is appealing but opens others non-trivial issues to get a satisfactory workflow we don't have solved yet.

Note that solutions often put decrypted secrets in the Nix store, which of course doesn't fit for really sensitive data, that's why we use the term of "private" instead of "secrets" : data we simply don't want to be world readable on a public git repository.

### Solution

The elaborated solution is a function that decrypt impurely (to access the user private key) sops files and put it unencrypted in the store. `__noChroot = true;` combined with `nixConfig.sandbox = "relaxed";` is the thing that allows this impurity. This way privates can be accessed at Nix evaluation time from any Nix code.

An important point though is that we may want to work on a NixOS config we deploy to another machine. In such a case build locality becomes critical as the private key is only available on one machine. In our specific case :
- For prod cluster, derivations are built directly on the servers (deploy-rs' `remoteBuild = true;`) because it obviously has a better internet throughput, no need to transit through our laptop.
- For dev cluster, derivations are built on our laptop and copied to the VMs that are also on the laptop (deploy-rs' `remoteBuild = false;`) because it allows caching the store and thus avoid repeating downloads, copies to the VMs are nearly instant.

The solution to handle both cases is the use of [`preferLocalBuild = true;`](https://nixos.org/manual/nix/stable/language/advanced-attributes.html#adv-attr-preferLocalBuild).

<details><summary>Implementation</summary>
<p>

- `lib/decrypt.nix` :
```nix
{ self, lib, pkgs }:

# Decrypt sops files impurly at evaluation time. This is suitable for private
# data but *not* for secret data since it will end up unencrypted in the store.
secrets:

with lib.asserts;
with lib.strings;

let
  decrypt = secret:
    assert assertMsg (builtins.isPath secret)
      ("Secret \"${toString secret}\" should be of type path"
      + optionalString (builtins.isString secret) " (Remove double quotes)"
      + ".");
    rec {
      name = removePrefix (toString self + "/secrets/") (toString secret);
      value = pkgs.runCommand "decrypted-${name}" {
        __noChroot = true;
        preferLocalBuild = true;
        src = secret;
      } ''
        mkdir -p $out/${removeSuffix (builtins.baseNameOf name) name}
        HOME=$(bash -c "realpath ~$(whoami)")
        ${pkgs.sops}/bin/sops -d ${secret} > $out/${name}
      '' + "/${name}";
    };

in builtins.listToAttrs (map decrypt secrets)
```
</p>
</details>

### Usage

- Somewhere :
```nix
let
  decrypt = import ./lib/decrypt.nix { inherit self lib pkgs; };

  sops-privates = decrypt [
    ./secrets/privates.json
    ];

  privates = builtins.fromJSON (builtins.readFile sops-privates."privates.json");

  inherit (privates)
    authorized-keys
    ip-dev-node1
    ip-dev-node2
    ip-prod-node2;
in
```

## NixOS private key

### Context

This is a particular case because it cannot be managed as a regular NixOS secret since it is the one used to decrypt all the others.

It is [usually](https://www.reddit.com/r/NixOS/comments/mvy86q/how_do_you_manage_your_private_keys/) left as a manual impure step, or as a Nix private. I believe [NixOps](https://nixops.readthedocs.io/en/latest/manual/nixops.html#command-nixops-send-keys) tackle it too with `send-keys`. [agenix-rekey](https://github.com/oddlama/agenix-rekey) was also announced on Reddit while I was writing this document.

### Solution

The elaborated solution here mainly consists in an automation of the key generation, the update of public key in `.sops.yaml` and secrets re-encryption.

Flows are slightly different between prod and dev as we definitely don't want servers private keys to ever leave the servers while we may want to store local VMs private keys as encrypted secrets accessible to administrators. This avoids the need of generating a new key each time VMs are destroyed, and also ease the shared use of the dev cluster by multiple developers.

The important part is to force secrets and private key to be always synchronized on a server, in other words to make the update of both the private key and the re-encrypted secrets an atomic action.

The solution is to put new private keys in a buffer directory, say `/var/lib/sops-private-key`, with a unique name, we choose a date-based one but a UUID or a hash could do the job too. And then, to use [NixOS activation scripts](https://nixos.org/manual/nixos/stable/#sec-activation-script) to symlink the actual private key to the location sops-nix will expect (often `/var/lib/sops-nix/key.txt` for age keys). And finally delete the old private keys.

Sops-nix also rely on activation scripts to activate secrets, so we must ensure private key activation occurs before sops-nix scripts and the old keys cleaning is better done once sops-nix finished successfully.

In the end, updating a private key on a server will take effect only once we also deployed a NixOS config with re-encrypted secrets, with deploy-rs for instance.

<details><summary>Implementation</summary>
<p>

- `modules/load-private-key.nix` :
```nix
{ config
, ...
}:

let
#  cfg = config.load-private-key;
  opt = options.load-private-key;

in {
  options.load-private-key = {
    from = mkOption {
      type = types.str;
      default = "/var/lib/sops-private-key";
      description = lib.mdDoc ''
        Path from which to load the updated sops private key
      '';
    };
  };

  config = {
    sops = {
      age.generateKey = false;
      age.sshKeyPaths = [];
      gnupg.sshKeyPaths = [];
    };

    system.activationScripts =
    let
      last-key = "$(find ${opt.from} -type f | sort | tail -1)";
    in {
      # _aaa_ is a way to force this script to run before sops-nix' setupSecrets
      # because there is currently no good way to do it as simple as for the
      # other way with deps.
      # https://discourse.nixos.org/t/reverse-dependency-in-the-activation-script/6186
      _aaa_load-private-key = {
        supportsDryActivation = false;
        text = ''
          if [ ! -d ${opt.from} ] \
          || [ "$(find ${opt.from} -type f | wc -l)" = 0 ]
          then
            echo "ERROR : There is no sops private key to symlink to ${config.sops.age.keyFile}, deploy one first." >&2
            exit 1
          fi

          mkdir -p /var/lib/sops-nix
          ln -sfn "${last-key}" ${config.sops.age.keyFile}
        '';nofreedisk-deploy-dev-node1
      };

      clean-old-private-keys = {
        supportsDryActivation = false;
        deps = [ "setupSecrets" ];
        text = ''
            rm -f $(find ${opt.from} -type f -not -wholename "${last-key}")
        '';
      };
    };
  };
}
```

- `lib/tasks/update-secrets.nix` :
```nix
{ pkgs
, sops ? pkgs.sops
}:

pkgs.writers.writeBashBin "update-secrets" ''
  for secret in $(find secrets -type f)
  do
    ${pkgs.sops}/bin/sops updatekeys -y $secret
  done
''
```

- `lib/tasks/sendkey-sops-dev.nix` :
```nix
{ lib
, pkgs
, sops ? pkgs.sops
}:

{ task-name
, ip
, file
, destination ? "/var/lib/sops-private-key"
}:

with lib.asserts;
assert assertMsg (builtins.isString file)
  "The file \"${toString file}\" containing the sops private key should be given as a string.";

(pkgs.writers.writeBashBin task-name ''
  set -e
  ssh root@${ip} 'mkdir -p ${destination}'
  CLEAR_KEY=$(tempfile)
  ${pkgs.sops}/bin/sops -d --output ''${CLEAR_KEY} ${file}
  scp ''${CLEAR_KEY} root@${ip}:${destination}/$(date -u +%F-%H-%M-%S)
  rm -f ''${CLEAR_KEY}
'')
```

- `lib/tasks/keygen-sops-dev.nix` :
```nix
{ lib
, pkgs
, age ? pkgs.age
, sops ? pkgs.sops
}:

{ task-name
, ip
, key
, file
, destination ? "/var/lib/sops-private-key"
}:

with lib.asserts;
assert assertMsg (builtins.isString file)
  "The file \"${toString file}\" containing the sops private key should be given as a string.";

(pkgs.writers.writeBashBin task-name ''
  set -e
  ssh root@${ip} 'mkdir -p ${destination}'
  rm -f ${file}
  ${pkgs.age}/bin/age-keygen -o ${file}
  NEW_PUBKEY=$(${pkgs.age}/bin/age-keygen -y ${file})
  sed -i .sops.yaml -e "s/&${key}.*$/\&${key} ''${NEW_PUBKEY}/"
  scp ${file} root@${ip}:${destination}/$(date -u +%F-%H-%M-%S)
  ${pkgs.sops}/bin/sops -e -i ${file}
  ${lib.update-secrets}/bin/nofreedisk-update-secrets
'')
```

- `lib/tasks/keygen-sops-prod.nix` :
```nix
{ lib
, pkgs
, age ? pkgs.age
}:

{ task-name
, ip
, key
, user
, destination ? "/var/lib/sops-private-key"
}:

let
  last-key = "$(find ${destination} -type f | sort | tail -1)";
in (lib.make-task task-name ''
  set -e
  ssh ${user}@${ip} '
    sudo mkdir -p ${destination}
    sudo ${pkgs.age}/bin/age-keygen -o ${destination}/$(date -u +%F-%H-%M-%S)'
  NEW_PUBKEY=$(ssh ${user}@${ip} 'sudo ${pkgs.age}/bin/age-keygen -y "${last-key}"')
  sed -i .sops.yaml -e "s/&${key}.*$/\&${key} ''${NEW_PUBKEY}/"
  ${lib.update-secrets}/bin/nofreedisk-update-secrets
'')
```

</p>
</details>

### Usage

- NixOS config :
```nix
  imports = [
    ./modules/load-private-key.nix
  ];
```

- `flake.nix` :
```nix
outputs.packages.${system} = {
  sendkey-sops-dev-node1 = lib.sendkey-sops-dev {
    task-name = "sendkey-sops-dev-node1";
    ip = ip-dev-node1;
    file = "secrets/sops-private-dev-node1";
  };
  keygen-sops-dev-node1 = lib.keygen-sops-dev {
    task-name = "keygen-sops-dev-node1";
    ip = ip-dev-node1;
    key = "sops-private-dev-node1";
    file = "secrets/sops-private-dev-node1";
  };
  keygen-sops-prod-node1 = lib.keygen-sops-prod {
    task-name = "keygen-sops-prod-node1";
    ip = ip-dev-node1;
    key = "sops-private-dev-node1";
    user = "root";
  };
};
```

- `nofreedisk$` :
```sh
# To upload its private key to a dev VM.
sendkey-sops-dev-node1

# To regen & upload its private key to as dev VM.
keygen-sops-dev-node1

# To regen in place a real server's private key & download its public key.
keygen-sops-prod-node1
```

## Terraform backend configuration

### Context

Due to its precedence in time over everything else, we cannot rely on a provider like the sops one to provide backend settings, neither to the regular variable mechanism. Plus, something like [terranix](https://github.com/terranix/terranix) won't help here because backend settings are sensitive, so we rather avoid to hardcode those in local Terraform files, nor in the Nix store. It is also the reason why [this kind](https://github.com/hashicorp/terraform/issues/13022#issuecomment-294262392) of trick to force variable usage is not the best solution. Hashicorp [recommends](https://developer.hashicorp.com/terraform/language/settings/backends/configuration#credentials-and-sensitive-data) providing backend settings through environment variables.

While searching on the internet, I haven't seen anything that helps to automate Terraform backend setting provision in the context of a Nix wrapped project so here we go.

### Solution

The target used to interact with Terraform has to have the ability to pass to Terraform:
- Secret backend settings coming from sops without leaving traces in the Nix store nor in Terraform files
- Other backend settings coming from Nix
- Regular variables coming from Nix

Backend settings use environment variables, and regular variables are converted to a JSON file, then passed using `-var-file`.

The thing that is a little tricky in this code is about regular `path` to `string` [conversion issues](https://stackoverflow.com/a/43850372) in Nixlang (`${path}` vs. `toString path`) that can end up with secret paths not matching anymore their associated path-based rules in `.sops.yaml`.

<details><summary>Implementation</summary>
<p>

- `lib/tasks/terraform.nix` :
```nix
{ lib
, pkgs
, sops ? pkgs.sops
, terraform ? pkgs.terraform
}:

{ name
, chdir ? "."
, vars ? null
, backend-direct-config ? {}
, backend-secret-config ? null
, auto-approve ? false
}:

with lib.asserts;
assert assertMsg (builtins.isString chdir)
  ("The chdir directory \"${toString chdir}\" from which Terraform will be run"
  + "should be given as a string.");

let
  arg-auto-approve = lib.optionalString auto-approve "-auto-approve";

  arg-vars =
  let
    fix-paths-attrs = key: value:
      if (lib.isList value)
      then (map fix-paths-list value)
      else toString value;
    fix-paths-list = element:
      if (lib.isAttrs element)
      then (lib.mapAttrsRecursive fix-paths-attrs element)
      else toString element;
    make-tfvars = name: vars:
      (builtins.toFile "${name}.tfvars.json"
      (builtins.toJSON (lib.mapAttrsRecursive fix-paths-attrs vars)));
  in lib.optionalString (vars != null)
    "-var-file=\"${make-tfvars name vars}\"";

  export-secret-config = lib.optionalString (backend-secret-config != null) ''
    DECRYPTED="$(${sops}/bin/sops -d --output-type dotenv ${toString backend-secret-config})"
    export $DECRYPTED
  '';

  export-direct-config = lib.concatLines (lib.mapAttrsToList
    (name: value:
      "export ${name}=\"${toString value}\""
    ) backend-direct-config);

in {
  "terraform-apply-${name}" = (pkgs.writers.writeBashBin "terraform-apply-${name}" ''
    set -e
    ${export-secret-config}
    ${export-direct-config}
    ${terraform}/bin/terraform -chdir=${chdir} init ${arg-vars} -reconfigure
    ${terraform}/bin/terraform -chdir=${chdir} apply ${arg-vars} ${arg-auto-approve}
  '');

  "terraform-destroy-${name}" = (pkgs.writers.writeBashBin "terraform-destroy-${name}" ''
    set -e
    ${export-secret-config}
    ${export-direct-config}
    ${terraform}/bin/terraform -chdir=${chdir} destroy ${arg-vars} ${arg-auto-approve}
  '');
}
```
</p>
</details>

### Usage

- `secrets/terraform-backend-dev.json` :
```json
{
	"CONSUL_HTTP_TOKEN": "token"
}
```

- `flake.nix` :
```nix
outputs.packages.${system} = {
} // lib.terraform {
  name = "dev";
  auto-approve = true;
  chdir = "terraform";
  backend-secret-config = ./secrets/terraform-backend-dev.json;
  backend-direct-config = {
    CONSUL_HTTP_ADDR = "${ip-dev-node1}:8500";
  };
  vars = {
    sops_file = ./secrets/terraform.json;
    nomad_address = "http://${ip-dev-node1}:4646";
  };
};
```

- `terraform/main.tf` :
```
terraform {
  backend "consul" {
#    address = "MUST_BE_SET_THROUGH_ENV_VAR"
#    access_token = ""
    scheme = "http"
    path = "terraform/tfstate"
  }
}
```

## Terraform secrets

### Context

This is well suited, many providers exist to access secrets in a secure store, such as Vault for instance. Let's use [the sops one](https://registry.terraform.io/providers/carlpett/sops/latest) to align with previous schemes.

### Usage

- `secrets/terraform.json` :
```json
{
	"example": "000"
}
```

- `terraform/main.tf` :
```
terraform {
  required_providers {
    sops = {
      source = "carlpett/sops"
      version = ">=0.7.2"
    }
  }
}
```

- `terraform/variables.tf` :
```
variable "sops_file" {
  type = string
  nullable = false
}
```

- `terraform/sops.tf` :
```
provider "sops" {}

data "sops_file" "secrets" {
  source_file = var.sops_file
}

output "example" {
  value = nonsensitive(data.sops_file.secrets.data["example"])
}
```

- Somewhere :
```nix
let
  terraform = pkgs.terraform_1.withPlugins (p: [
    p.sops
  ]);
in
```
