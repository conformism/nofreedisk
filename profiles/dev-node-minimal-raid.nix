{ state-version
, authorized-keys
}:

{ modulesPath
, lib
, pkgs
, disko
, ...
}: {
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    (modulesPath + "/profiles/qemu-guest.nix")
    (modulesPath + "/virtualisation/vagrant-guest.nix")
    disko.nixosModules.disko
  ];
  disko.devices = import ./disks/dev-node-raid.nix { inherit lib; };
  boot.loader.grub = {
    devices = [
      "/dev/vda"
      "/dev/vdb"
    ];
    efiSupport = false;
    efiInstallAsRemovable = false;
  };
  boot.loader.systemd-boot.enable = false;
  system.stateVersion = state-version;
  services.openssh.enable = true;
  networking.useDHCP = true;
  networking.firewall.enable = pkgs.lib.mkDefault false;
  users.users.root.openssh.authorizedKeys.keys = authorized-keys;
  systemd.units."mdmonitor.service".enable = false;
#  boot.initrd.services.swraid.mdadmConf = ''
#    ARRAY /dev/md/raid1-root devices=/dev/vda1,/dev/vdb1
#  '';
}
