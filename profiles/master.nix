{ name
, ip
, stage
}:

{ modulesPath
, self
, pkgs
, ...
}: {
  imports = [
    (modulesPath + "/services/networking/nomad.nix")
    (modulesPath + "/services/web-servers/garage.nix")
#    (modulesPath + "/services/web-servers/minio.nix")
    self.nixosModules.vault
    self.nixosModules.consul
    self.nixosModules.consul-rotate-gossip-encryption-key
    self.nixosModules.consul-reset-acl
    self.nixosModules.consul-connect
  ];
  networking.hostName = name;
  services.nomad = {
    dropPrivileges = false;
    enable = true;
    extraPackages = [
      pkgs.consul
    ];
    settings = {
      bind_addr = "0.0.0.0";
      advertise = {
        http = ip;
        rpc = ip;
        serf = ip;
      };
      datacenter = "dc1";
#      plugin.raw_exec.config.enabled = true;
      server = {
        enabled = true;
        bootstrap_expect = 1;
      };
      client = {
        enabled = true;
        network_interface = "ens20";
        cni_path = "${pkgs.cni}/bin:${pkgs.cni-plugins}/bin";
      };
      consul = {
#        address = ip;
      };
      tls = {
        http = true;
        rpc = true;
        ca_file = "/run/secrets/certs/consul-agent-ca.pem";
        cert_file = "/run/secrets/certs/dc1-server-consul-0.pem";
        key_file = "/run/secrets/certs/dc1-server-consul-0-key.pem";
      };
      acl.enabled = false;
    };
  };
  sops = {
    secrets = {
      "certs/consul-agent-ca.pem" = {
        sopsFile = self + /secrets/dev/consul-agent-ca.pem;
        format = "binary";
        owner = "consul";
        group = "root";
        mode = "0444";
      };
      "certs/dc1-server-consul-0.pem" = {
        sopsFile = self + /secrets/dev/dc1-server-consul-0.pem;
        format = "binary";
        owner = "consul";
        group = "root";
        mode = "0444";
      };
      "certs/dc1-server-consul-0-key.pem" = {
        sopsFile = self + /secrets/dev/dc1-server-consul-0-key.pem;
        format = "binary";
        owner = "consul";
        group = "root";
        mode = "0444";
      };
      "consul-encrypt.json" = {
        sopsFile = self + "/secrets/" + stage + "/consul-encrypt.json";
        key = "";
        owner = "consul";
        restartUnits = [ "consul-rotate-gossip-encryption-key.service" ];
      };
      "consul-acl-initial-management.json" = {
        # Only has to be present on a leader server
        sopsFile = self + "/secrets/" + stage + "/consul-acl-initial-management.json";
        key = "";
        owner = "consul";
      };
    };
  };
  services.consul-rotate-gossip-encryption-key = {
    enable = true;
  };
  services.consul = {
    enable = true;
    webUi = true;
    extraConfigFiles = [
      "/run/secrets/consul-encrypt.json"
      "/run/secrets/consul-acl-initial-management.json"
    ];
    extraConfig = {
      server = true;
      bootstrap_expect = 1;
      bind_addr = ip;
      client_addr = "0.0.0.0";
      advertise_addr = ip;
      datacenter = "dc1";
      ports = {
        https = 8501;
        grpc = 8502;
        grpc_tls = 8503;
      };
      connect.enabled = true;
      acl = {
        enabled = true;
        default_policy = "allow";
        enable_token_persistence = true;
      };
      tls = {
        defaults = {
          ca_file = "/run/secrets/certs/consul-agent-ca.pem";
          cert_file = "/run/secrets/certs/dc1-server-consul-0.pem";
          key_file = "/run/secrets/certs/dc1-server-consul-0-key.pem";
          verify_incoming = true;
          verify_outgoing = true;
        };
        # Allow HTTPS for UI without adding cert to browser
        https.verify_incoming = false;
        # Allow connections from Envoy sidecar proxies to the local Consul agent.
        # https://github.com/hashicorp/consul/issues/13124
        # Looks fine while `true` with sidecars created by Nomad?
        grpc.verify_incoming = false;
      };
      auto_encrypt = {
        # This option enables auto_encrypt on the servers and allows them to automatically
        # distribute certificates from the service mesh CA to the clients. If enabled, the
        # server can accept incoming connections from both the built-in CA and the service
        # mesh CA, as well as their certificates
        allow_tls = true;
      };
    };
  };
  services.vault = {
    enable = true;
    package = pkgs.vault-bin;
    address = ip;
    storageBackend = "raft";
    listenerExtraConfig = ''
      address = "${ip}:8200"
    '';
    storageConfig = ''
      node_id = "node1"
    '';
    extraConfig = ''
      ui = true
      api_addr = "http://${ip}:8200"
      cluster_addr = "https://${ip}:8201"
    '';
  };
  environment.variables = {
    NOMAD_ADDR = "https://localhost:4646";
    CONSUL_HTTP_ADDR = "https://localhost:8501";
    # Enable TLS for CLI
    # Could also be set on operator machine with automated sops decryption
    # Should be different than agent's certs?
    CONSUL_CACERT = "/run/secrets/certs/consul-agent-ca.pem";
    CONSUL_CLIENT_CERT = "/run/secrets/certs/dc1-server-consul-0.pem";
    CONSUL_CLIENT_KEY = "/run/secrets/certs/dc1-server-consul-0-key.pem";
    CONSUL_HTTP_TOKEN = "$(${pkgs.jq}/bin/jq -r '.acl.tokens.initial_management' /run/secrets/consul-acl-initial-management.json)";
  };
#  environment.etc."fabio/fabio.properties".text = ''
#    registry.consul.addr = ${ip}:8500
#  '';

  services.jellyfin = {
    enable = true;
  };

  services.nginx = {
    enable = true;
    virtualHosts = {
      "color" = {
        listen = [
          {
            addr = "0.0.0.0";
            port = 5555;
          }
        ];
        root = pkgs.colored-webpage "GREEN";
      };
    };
  };

  services.consul-connect = {
    enable = true;
    services = {
      "color1" = {
        config = {
          service = {
            name = "color";
            id = "color1";
            port = 5555;
            connect = { sidecar_service = {}; };
            tags = [ "green" ];
          };
        };
      };
      "jellyfin1" = {
        config = {
          service = {
            name = "jellyfin";
            id = "jellyfin1";
            port = 8096;
            connect = { sidecar_service = {}; };
          };
        };
      };
      "ingress1" = {
        config = {
          service = {
            name = "ingress";
            id = "ingress1";
            kind = "ingress-gateway";
          };
        };
      };
      "api1" = {
        config = {
          service = {
            name = "api";
            id = "api1";
            kind = "api-gateway";
          };
        };
      };
      "consul-ui1" = {
        config = {
          service = {
            name = "consul-ui";
            id = "consul-ui1";
            port = 8500;
#            port = 8501;
#            # TODO Use TLS between service and sidecar proxy
#            # https://github.com/hashicorp/consul/issues/9646
            connect = { sidecar_service = {}; };
          };
        };
      };
    };
    sidecar-proxies = {
      "color1" = {
        adminBind = "localhost:19001";
      };
      "jellyfin1" = {
        adminBind = "localhost:19002";
      };
      "consul-ui1" = {
        adminBind = "localhost:19003";
      };
    };
    gateways = {
      "ingress1" = {
        adminBind = "localhost:19004";
      };
      "api1" = {
        adminBind = "localhost:19005";
      };
    };
  };
}
