{ state-version
, authorized-keys
}:

{ modulesPath
, lib
, pkgs
, disko
, ...
}: {
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    (modulesPath + "/profiles/qemu-guest.nix")
#    (modulesPath + "/profiles/headless.nix")
    (modulesPath + "/virtualisation/vagrant-guest.nix")
    disko.nixosModules.disko
  ];
  disko.devices = import ./disks/dev-node.nix { inherit lib; };
  boot.loader.grub = {
    devices = [
      "/dev/vda"
    ];
  };
  system.stateVersion = state-version;
  services.openssh.enable = true;
  networking.useDHCP = true;
  networking.firewall.enable = pkgs.lib.mkDefault false;
  users.users.root.openssh.authorizedKeys.keys = authorized-keys;
#  systemd.units."mdmonitor.service".enable = false;
#  boot.initrd.services.swraid.mdadmConf = ''
#    MAILADDR root
#  '';
  environment.etc."mdadm.conf".text = ''
    MAILADDR root
  '';
}
