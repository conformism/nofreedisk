{ self
, lib
, pkgs
, sops-nix
, ...
}: {
  imports = [
    sops-nix.nixosModules.sops
    self.nixosModules.load-private-key
    self.nixosModules.sops-example
  ];
#  system.stateVersion = state-version;
  nix.settings = {
    experimental-features = [ "nix-command" "flakes" ];
    auto-optimise-store = true;
  };
  nixpkgs.overlays = [ self.overlays.pkgs ];
  nixpkgs.config.allowlistedLicenses = [ lib.licenses.bsl11 ];
  sops = {
    age.keyFile = "/var/lib/sops-nix/key.txt";
#    defaultSopsKey = "";
    defaultSopsFormat = "json";
    defaultSopsFile = self + /secrets/nixos.json;
    secrets = {
      "nixos.json" = {
        sopsFile = self + /secrets/nixos.json;
        format = "json";
        key = "";
      };
      "example" = {};
    };
  };
  virtualisation.docker.enable = true;
  virtualisation.oci-containers.backend = "docker";
  environment.systemPackages = with pkgs; [
    parted
    bat
    vim
    tree
    htop
    git
    curl
    damon
    wander
    podman
#    minio-client
#    vault
  ];
  zramSwap.enable = true;
  networking.nameservers = [
    "0.0.0.0"
    "8.8.8.8"
  ];
  networking.firewall = {
    enable = false;
    allowedTCPPorts = [
      80
      4646 4647 4648 # Nomad
      8500 # Consul
      9998 9999 # Fabio
      8200 # Vault
      8443 # Ceph
      9000 9001 # Minio
      2812 # Monit
      3900 3901 3902 # Garage
    ];
  };
  # TODO /store RO, everywhere else noexec
#  fileSystems."/".options = [ "noexec" ];

  system.activationScripts.report-changes = ''
    ${pkgs.nvd}/bin/nvd \
      --nix-bin-dir ${pkgs.nix}/bin \
      --color always \
      diff $(ls -dv /nix/var/nix/profiles/system-*-link | tail -2)
  '';
}
