{ name
, ip
, ip-server
, stage
}:

{ modulesPath
, self
, pkgs
, ...
}: {
  imports = [
    (modulesPath + "/services/networking/nomad.nix")
    (modulesPath + "/services/web-servers/garage.nix")
    self.nixosModules.consul
    self.nixosModules.consul-connect
  ];
  networking.hostName = name;
  services.nomad = {
    dropPrivileges = false;
    enable = true;
    extraPackages = [
      pkgs.consul
    ];
    settings = {
      bind_addr = "0.0.0.0";
      advertise = {
        http = ip;
        rpc = ip;
        serf = ip;
      };
      datacenter = "dc1";
#      plugin.raw_exec.config.enabled = true;
      client = {
        enabled = true;
        network_interface = "ens20";
        cni_path = "${pkgs.cni}/bin:${pkgs.cni-plugins}/bin";
      };
      consul = {
#        address = ip;
      };
      tls = {
        http = true;
        rpc = true;
        ca_file = "/run/secrets/certs/consul-agent-ca.pem";
        cert_file = "/run/secrets/certs/dc1-client-consul-0.pem";
        key_file = "/run/secrets/certs/dc1-client-consul-0-key.pem";
      };
      acl.enabled = false;
    };
  };
  sops = {
    secrets = {
      "certs/consul-agent-ca.pem" = {
        sopsFile = self + /secrets/dev/consul-agent-ca.pem;
        format = "binary";
        owner = "consul";
        mode = "0444";
      };
      "certs/dc1-client-consul-0.pem" = {
        sopsFile = self + /secrets/dev/dc1-client-consul-0.pem;
        format = "binary";
        owner = "consul";
        mode = "0444";
      };
      "certs/dc1-client-consul-0-key.pem" = {
        sopsFile = self + /secrets/dev/dc1-client-consul-0-key.pem;
        format = "binary";
        owner = "consul";
        mode = "0444";
      };
      "consul-encrypt.json" = {
        sopsFile = self + "/secrets/" + stage + "/consul-encrypt.json";
        key = "";
        owner = "consul";
      };
      "consul-acl-initial-management.json" = {
        # Only has to be present on a leader server
        sopsFile = self + "/secrets/" + stage + "/consul-acl-initial-management.json";
        key = "";
        owner = "consul";
      };
    };
  };
  services.consul = {
    enable = true;
    webUi = true;
    extraConfigFiles = [
      "/run/secrets/consul-encrypt.json"
    ];
    extraConfig = {
      bind_addr = ip;
      client_addr = "0.0.0.0";
      advertise_addr = ip;
      datacenter = "dc1";
      retry_join = [ ip-server ];
      ports = {
        https = 8501;
        grpc = 8502;
        grpc_tls = 8503;
      };
      acl = {
        enabled = true;
        default_policy = "allow";
        enable_token_persistence = true;
      };
      tls = {
        defaults = {
          ca_file = "/var/run/secrets/certs/consul-agent-ca.pem";
          # With auto-encryption, you can configure the Consul servers to automatically
          # distribute certificates to the clients. To use this feature, you will need to
          # configure clients to automatically get the certificates from the server.
          # Note, this method stores the certificates in memory, they are not persisted.
#          cert_file = "/var/run/secrets/certs/dc1-client-consul-0.pem";
#          key_file = "/var/run/secrets/certs/dc1-client-consul-0-key.pem";
          verify_incoming = true;
          verify_outgoing = true;
        };
        # Allow HTTPS for UI without adding cert to browser
        https.verify_incoming = false;
        # Allow connections from Envoy sidecar proxies to the local Consul agent.
        # Looks fine while `true` with sidecars created by Nomad?
        # https://github.com/hashicorp/consul/issues/13124
        grpc.verify_incoming = false;
      };
      auto_encrypt = {
        # Allows the client to request the service mesh CA and certificates from the servers,
        # for encrypting RPC communication. The client will make the request to any servers
        # listed in the -retry-join option. This requires that every server to have
        # auto_encrypt.allow_tls enabled. When both auto_encrypt options are used, it allows
        # clients to receive certificates that are generated on the servers. If the -server-port
        # is not the default one, it has to be provided to the client as well. Usually this
        # is discovered through LAN gossip, but auto_encrypt provision happens before the
        # information can be distributed through gossip. The most secure auto_encrypt setup
        # is when the client is provided with the built-in CA, verify_server_hostname is
        # turned on, and when an ACL token with node.write permissions is setup. It is also
        # possible to use auto_encrypt with a CA and ACL, but without verify_server_hostname,
        # or only with a ACL enabled, or only with CA and verify_server_hostname, or only with
        # a CA, or finally without a CA and without ACL enabled. In any case, the communication
        # to the auto_encrypt endpoint is always TLS encrypted.
        tls = true;
      };
    };
  };
  environment.variables = {
    NOMAD_ADDR = "http://${ip}:4646";
    CONSUL_HTTP_ADDR = "https://localhost:8501";
    CONSUL_CACERT = "/run/secrets/certs/consul-agent-ca.pem";
    CONSUL_CLIENT_CERT = "/run/secrets/certs/dc1-client-consul-0.pem";
    CONSUL_CLIENT_KEY = "/run/secrets/certs/dc1-client-consul-0-key.pem";
    CONSUL_HTTP_TOKEN = "$(${pkgs.jq}/bin/jq -r '.acl.tokens.initial_management' /run/secrets/consul-acl-initial-management.json)";
  };

  containers."color2" = {
    autoStart = true;
    ephemeral = true;
    config = { config, pkgs, ... }: {
      nixpkgs.overlays = [ self.overlays.pkgs ];
      services.nginx = {
        enable = true;
        virtualHosts = {
          "color" = {
            listen = [
              {
                addr = "0.0.0.0";
                port = 5555;
              }
            ];
            root = pkgs.colored-webpage "BLUE";
          };
        };
      };
    };
  };

  containers.jellyfin = {
    ephemeral = true;
    autoStart = true;
    bindMounts = {
      secrets = {
        mountPoint = "/run/secrets";
        hostPath = "/run/secrets";
        isReadOnly = true;
      };
    };
    config = { config, pkgs, ... }: {
      nixpkgs.overlays = [ self.overlays.pkgs ];
      imports = [
        self.nixosModules.consul
        self.nixosModules.consul-connect
      ];
      environment.variables = {
        CONSUL_HTTP_ADDR = "https://localhost:8501";
        CONSUL_CACERT = "/run/secrets/certs/consul-agent-ca.pem";
        CONSUL_CLIENT_CERT = "/run/secrets/certs/dc1-server-consul-0.pem";
        CONSUL_CLIENT_KEY = "/run/secrets/certs/dc1-server-consul-0-key.pem";
        CONSUL_HTTP_TOKEN = "$(${pkgs.jq}/bin/jq -r '.acl.tokens.initial_management' /run/secrets/consul-acl-initial-management.json)";
      };
      services.jellyfin = {
        enable = true;
      };
      services.consul-connect = {
        enable = true;
        avoidSystemdDependenciesToConsul = true;
        sidecar-proxies = {
          "jellyfin2" = {
            adminBind = "localhost:19002";
          };
        };
        services = {
          "jellyfin2" = {
            config = {
              service = {
                name = "jellyfin";
                id = "jellyfin2";
                port = 8096;
                connect = { sidecar_service = {}; };
              };
            };
          };
        };
      };
    };
  };

  virtualisation.oci-containers.containers."2048-game1" = {
#    imageFile = pkgs.dockerTools.buildLayeredImage {};
    image = "alexwhen/docker-2048";
    ports = [
      "8099:80"
    ];
  };

  containers."2048-game1" = {
    ephemeral = true;
    autoStart = true;
    bindMounts = {
      secrets = {
        mountPoint = "/run/secrets";
        hostPath = "/run/secrets";
        isReadOnly = true;
      };
    };
    config = { config, pkgs, ... }: {
      nixpkgs.overlays = [ self.overlays.pkgs ];
      imports = [
        self.nixosModules.consul
        self.nixosModules.consul-connect
      ];
      environment.variables = {
        CONSUL_HTTP_ADDR = "https://localhost:8501";
        CONSUL_CACERT = "/run/secrets/certs/consul-agent-ca.pem";
        CONSUL_CLIENT_CERT = "/run/secrets/certs/dc1-server-consul-0.pem";
        CONSUL_CLIENT_KEY = "/run/secrets/certs/dc1-server-consul-0-key.pem";
        CONSUL_HTTP_TOKEN = "$(${pkgs.jq}/bin/jq -r '.acl.tokens.initial_management' /run/secrets/consul-acl-initial-management.json)";
      };
      services.consul-connect = {
        enable = true;
        avoidSystemdDependenciesToConsul = true;
        sidecar-proxies = {
          "2048-game1" = {
            adminBind = "localhost:19003";
          };
        };
        services = {
          "2048-game1" = {
            config = {
              service = {
                name = "2048-game";
                id = "2048-game1";
                port = 8099;
                connect = { sidecar_service = {}; };
              };
            };
          };
        };
      };
    };
  };

  containers."color3-service" = {
    ephemeral = true;
    autoStart = true;
    bindMounts = {
      secrets = {
        mountPoint = "/run/secrets";
        hostPath = "/run/secrets";
        isReadOnly = true;
      };
    };
    config = { config, pkgs, ... }: {
      nixpkgs.overlays = [ self.overlays.pkgs ];
      imports = [
        self.nixosModules.consul
        self.nixosModules.consul-connect
      ];
      environment.variables = {
        CONSUL_HTTP_ADDR = "https://localhost:8501";
        CONSUL_CACERT = "/run/secrets/certs/consul-agent-ca.pem";
        CONSUL_CLIENT_CERT = "/run/secrets/certs/dc1-server-consul-0.pem";
        CONSUL_CLIENT_KEY = "/run/secrets/certs/dc1-server-consul-0-key.pem";
        CONSUL_HTTP_TOKEN = "$(${pkgs.jq}/bin/jq -r '.acl.tokens.initial_management' /run/secrets/consul-acl-initial-management.json)";
      };
      services.consul-connect = {
        enable = true;
        avoidSystemdDependencies = true;
        services = {
          "color3" = {
            config = {
              service = {
                name = "color";
                id = "color3";
                port = 5665;
                connect = { sidecar_service = {}; };
                tags = [ "yellow" ];
              };
            };
          };
        };
      };
    };
  };

  containers."color3-proxy" = {
    ephemeral = true;
    autoStart = true;
    bindMounts = {
      secrets = {
        mountPoint = "/run/secrets";
        hostPath = "/run/secrets";
        isReadOnly = true;
      };
    };
    config = { config, pkgs, ... }: {
      nixpkgs.overlays = [ self.overlays.pkgs ];
      imports = [
        self.nixosModules.consul
        self.nixosModules.consul-connect
      ];
      environment.variables = {
        CONSUL_HTTP_ADDR = "https://localhost:8501";
        CONSUL_CACERT = "/run/secrets/certs/consul-agent-ca.pem";
        CONSUL_CLIENT_CERT = "/run/secrets/certs/dc1-server-consul-0.pem";
        CONSUL_CLIENT_KEY = "/run/secrets/certs/dc1-server-consul-0-key.pem";
        CONSUL_HTTP_TOKEN = "$(${pkgs.jq}/bin/jq -r '.acl.tokens.initial_management' /run/secrets/consul-acl-initial-management.json)";
      };
      services.consul-connect = {
        enable = true;
        avoidSystemdDependencies = true;
        sidecar-proxies = {
          "color3" = {
            adminBind = "localhost:19004";
            service = "color";
            id = "color3";
          };
        };
      };
    };
  };

  services.nginx = {
    enable = true;
    virtualHosts = {
      "color" = {
        listen = [
          {
            addr = "0.0.0.0";
            port = 5665;
          }
        ];
        root = pkgs.colored-webpage "YELLOW";
      };
    };
  };

  services.consul-connect = {
    enable = true;
    services = {
      "color2" = {
        config = {
          service = {
            name = "color";
            id = "color2";
            port = 5555;
            connect = { sidecar_service = {}; };
            tags = [ "blue" ];
          };
        };
      };
    };
    sidecar-proxies = {
      "color2" = {
        adminBind = "localhost:19001";
      };
    };
  };
}
