{ name
, ip
, stage
, dns-email
#, authorized-keys
}:

{ modulesPath
, self
, pkgs
, ...
}: {
  imports = [
    self.nixosModules.consul-rotate-gossip-encryption-key
    self.nixosModules.consul-reset-acl
    self.nixosModules.consul-connect
    self.nixosModules.consul
  ];

  networking.hostName = name;

  sops = {
    secrets = {
      "certs/consul-agent-ca.pem" = {
        sopsFile = self + /secrets/prod/consul-agent-ca.pem;
        format = "binary";
        owner = "consul";
        group = "root";
        mode = "0444";
      };
      "certs/dc1-server-consul-0.pem" = {
        sopsFile = self + /secrets/prod/dc1-server-consul-0.pem;
        format = "binary";
        owner = "consul";
        group = "root";
        mode = "0444";
      };
      "certs/dc1-server-consul-0-key.pem" = {
        sopsFile = self + /secrets/prod/dc1-server-consul-0-key.pem;
        format = "binary";
        owner = "consul";
        group = "root";
        mode = "0444";
      };
      "consul-encrypt.json" = {
        sopsFile = self + "/secrets/" + stage + "/consul-encrypt.json";
        key = "";
        owner = "consul";
        restartUnits = [ "consul-rotate-gossip-encryption-key.service" ];
      };
      "consul-acl-initial-management.json" = {
        # Only has to be present on a leader server
        sopsFile = self + "/secrets/" + stage + "/consul-acl-initial-management.json";
        key = "";
        owner = "consul";
      };
      "acme-credentials-file.env" = {
        sopsFile = self + "/secrets/" + stage + "/acme-credentials-file.env";
        format = "dotenv";
#        owner = "acme";
#        group = "root";
        mode = "0444";
      };
    };
  };

  services.consul-rotate-gossip-encryption-key = {
    enable = true;
  };

  services.consul = {
    enable = true;
    extraConfigFiles = [
      "/run/secrets/consul-encrypt.json"
      "/run/secrets/consul-acl-initial-management.json"
    ];
    extraConfig = {
      server = true;
      bootstrap_expect = 1;
      bind_addr = ip;
      client_addr = "0.0.0.0";
      advertise_addr = ip;
      datacenter = "dc1";
      ports = {
        https = 8501;
        grpc = 8502;
        grpc_tls = 8503;
      };
      connect.enabled = true;
      acl = {
        enabled = true;
        default_policy = "allow";
        enable_token_persistence = true;
      };
      tls = {
        defaults = {
          ca_file = "/run/secrets/certs/consul-agent-ca.pem";
          cert_file = "/run/secrets/certs/dc1-server-consul-0.pem";
          key_file = "/run/secrets/certs/dc1-server-consul-0-key.pem";
          verify_incoming = true;
          verify_outgoing = true;
        };
        https.verify_incoming = false;
        grpc.verify_incoming = false;
      };
      auto_encrypt = {
        allow_tls = true;
      };
      domain = "nofreedisk";

      ui_config = {
        enabled = true;
        metrics_provider = "prometheus";
        metrics_proxy = {
          base_url = "http://localhost:9090";
        };
      };
    };
  };

  environment.variables = {
    CONSUL_HTTP_ADDR = "https://localhost:8501";
    CONSUL_CACERT = "/run/secrets/certs/consul-agent-ca.pem";
    CONSUL_CLIENT_CERT = "/run/secrets/certs/dc1-server-consul-0.pem";
    CONSUL_CLIENT_KEY = "/run/secrets/certs/dc1-server-consul-0-key.pem";
    CONSUL_HTTP_TOKEN = "$(${pkgs.jq}/bin/jq -r '.acl.tokens.initial_management' /run/secrets/consul-acl-initial-management.json)"; #TODO fix : cannot run if
  };

  containers."prometheus" = {
    ephemeral = true;
    autoStart = true;
    config = { config, pkgs, ... }: {
      nixpkgs.overlays = [ self.overlays.pkgs ];
      networking.firewall.enable = false;

      services.prometheus = {
        enable = true;
        port = 9090;
        retentionTime = "2d";
        scrapeConfigs = [
          {
            job_name = "Sidecar proxies";
            static_configs = [{
              targets = [
                "localhost:18001"
                "localhost:18002"
                "localhost:18003"
                "localhost:18004"
                "localhost:18005"
                "localhost:18006"
                "localhost:18007"
                "localhost:18008"
              ];
            }];
          }
        ];
      };
    };
  };

  services.jellyfin = {
    enable = true;
  };

  services.minio = {
    enable = true;
    region = "global";
  };

  services.nginx = {
    enable = true;
    virtualHosts = {
      "color" = {
        listen = [
          {
            addr = "0.0.0.0";
            port = 5555;
          }
        ];
        root = pkgs.colored-webpage "GREEN";
      };
    };
  };

  virtualisation.oci-containers.containers."2048-game1" = {
    image = "alexwhen/docker-2048";
    ports = [
      "8099:80"
    ];
  };

  services.consul-connect = {
    enable = true;
    services = {
      "color1" = {
        config = {
          service = {
            name = "color";
            id = "color1";
            port = 5555;
            connect.sidecar_service.proxy.config = {
              envoy_prometheus_bind_addr = "0.0.0.0:18001";
            };
            tags = [ "green" ];
          };
        };
      };
      "jellyfin1" = {
        config = {
          service = {
            name = "jellyfin";
            id = "jellyfin1";
            port = 8096;
            connect.sidecar_service.proxy.config = {
              envoy_prometheus_bind_addr = "0.0.0.0:18002";
            };
          };
        };
      };
      "minio1" = {
        config = {
          service = {
            name = "minio";
            id = "minio1";
            port = 9001;
            connect.sidecar_service.proxy.config = {
              envoy_prometheus_bind_addr = "0.0.0.0:18003";
            };
          };
        };
      };
      "consul-ui1" = {
        config = {
          service = {
            name = "consul-ui";
            id = "consul-ui1";
            port = 8500;
            connect.sidecar_service.proxy.config = {
              envoy_prometheus_bind_addr = "0.0.0.0:18004";
            };
          };
        };
      };
      "2048-game1" = {
        config = {
          service = {
            name = "2048-game";
            id = "2048-game1";
            port = 8099;
            connect.sidecar_service.proxy.config = {
              envoy_prometheus_bind_addr = "0.0.0.0:18005";
            };
          };
        };
      };
      "prometheus1" = {
        config = {
          service = {
            name = "prometheus";
            id = "prometheus1";
            port = 9090;
            connect.sidecar_service.proxy.config = {
              envoy_prometheus_bind_addr = "0.0.0.0:18006";
            };
          };
        };
      };
      "ingress1" = {
        config = {
          service = {
            name = "ingress";
            id = "ingress1";
            kind = "ingress-gateway";
            proxy.config = {
              envoy_prometheus_bind_addr = "0.0.0.0:18007";
            };
          };
        };
      };
      "api1" = {
        config = {
          service = {
            name = "api";
            id = "api1";
            kind = "api-gateway";
            proxy.config = {
              envoy_prometheus_bind_addr = "0.0.0.0:18008";
            };
          };
        };
      };
    };
    sidecar-proxies = {
      "color1".adminBind = "localhost:19001";
      "jellyfin1".adminBind = "localhost:19002";
      "minio1".adminBind = "localhost:19003";
      "consul-ui1".adminBind = "localhost:19004";
      "2048-game1".adminBind = "localhost:19005";
      "prometheus1".adminBind = "localhost:19006";
    };
    gateways = {
      "ingress1".adminBind = "localhost:19007";
      "api1".adminBind = "localhost:19008";
    };
    inline-certificates = {
      "nofreedisk".useACMEHost = "nofreedisk.fr.eu.org";
    };
  };

  security.acme = {
    acceptTerms = true;
    defaults.email = dns-email;
    certs = {
      "nofreedisk.fr.eu.org" = {
        domain = "nofreedisk.fr.eu.org";
        extraDomainNames = [ "*.nofreedisk.fr.eu.org" ];
        dnsProvider = "cloudflare";
        dnsResolver = "173.245.58.248:53";
        credentialsFile = "/run/secrets/acme-credentials-file.env";
      };
    };
  };
}
